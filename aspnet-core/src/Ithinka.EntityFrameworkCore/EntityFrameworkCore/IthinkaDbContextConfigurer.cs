using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace Ithinka.EntityFrameworkCore
{
    public static class IthinkaDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<IthinkaDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<IthinkaDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}

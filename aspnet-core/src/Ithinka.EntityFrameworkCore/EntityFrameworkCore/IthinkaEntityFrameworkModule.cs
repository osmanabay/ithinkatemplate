﻿using Abp.EntityFrameworkCore.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Abp.Zero.EntityFrameworkCore;
using Ithinka.EntityFrameworkCore.Seed;
using Microsoft.EntityFrameworkCore;

namespace Ithinka.EntityFrameworkCore
{
    [DependsOn(
        typeof(IthinkaCoreModule), 
        typeof(AbpZeroCoreEntityFrameworkCoreModule))]
    public class IthinkaEntityFrameworkModule : AbpModule
    {
        /* Used it tests to skip dbcontext registration, in order to use in-memory database of EF Core */
        public bool SkipDbContextRegistration { get; set; }

        public bool SkipDbSeed { get; set; }

        public override void PreInitialize()
        {
            if (!SkipDbContextRegistration)
            {
                Configuration.Modules.AbpEfCore().AddDbContext<IthinkaDbContext>(options =>
                {
                    options.DbContextOptions.UseLazyLoadingProxies();

                    if (options.ExistingConnection != null)
                    {
                        IthinkaDbContextConfigurer.Configure(options.DbContextOptions, options.ExistingConnection);
                    }
                    else
                    {
                        IthinkaDbContextConfigurer.Configure(options.DbContextOptions, options.ConnectionString);
                    }
                });
            }
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(IthinkaEntityFrameworkModule).GetAssembly());
        }

        public override void PostInitialize()
        {
            if (!SkipDbSeed)
            {
                SeedHelper.SeedHostDb(IocManager);
            }
        }
    }
}

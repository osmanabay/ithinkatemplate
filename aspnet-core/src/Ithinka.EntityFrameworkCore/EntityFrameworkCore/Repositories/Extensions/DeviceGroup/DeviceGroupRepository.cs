﻿using Abp.EntityFrameworkCore;
using Ithinka.Domain.Entities;

namespace Ithinka.EntityFrameworkCore.Repositories
{
    public class DeviceGroupRepository : IthinkaRepositoryBase<DeviceGroup, int>, IDeviceGroupRepository
    {
        public DeviceGroupRepository(IDbContextProvider<IthinkaDbContext> dbContextProvider) 
            : base(dbContextProvider)
        {
        }
    }
}

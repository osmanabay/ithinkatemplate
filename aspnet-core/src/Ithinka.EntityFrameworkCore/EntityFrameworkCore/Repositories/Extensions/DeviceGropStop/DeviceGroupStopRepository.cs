﻿using Abp.EntityFrameworkCore;
using Ithinka.Domain.Entities;

namespace Ithinka.EntityFrameworkCore.Repositories.Extensions.DeviceGropStop
{
    public class DeviceGroupStopRepository : IthinkaRepositoryBase<DeviceGroupStop, int>, 
        IDeviceGroupStopRepository
    {
        public DeviceGroupStopRepository(IDbContextProvider<IthinkaDbContext> dbContextProvider) 
            : base(dbContextProvider)
        {
        }
    }
}

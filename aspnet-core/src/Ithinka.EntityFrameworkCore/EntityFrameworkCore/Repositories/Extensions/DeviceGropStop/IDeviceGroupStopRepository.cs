﻿using Abp.Domain.Repositories;
using Ithinka.Domain.Entities;

namespace Ithinka.EntityFrameworkCore.Repositories.Extensions.DeviceGropStop
{
    public interface IDeviceGroupStopRepository: IRepository<DeviceGroupStop,int>
    {
    }
}

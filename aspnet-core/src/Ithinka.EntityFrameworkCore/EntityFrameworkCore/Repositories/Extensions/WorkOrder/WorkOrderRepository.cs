﻿using Abp.EntityFrameworkCore;
using Ithinka.Domain.Entities;

namespace Ithinka.EntityFrameworkCore.Repositories
{
    public class WorkOrderRepository : IthinkaRepositoryBase<WorkOrder, int>, IWorkOrderRepository
    {
        public WorkOrderRepository(IDbContextProvider<IthinkaDbContext> dbContextProvider) 
            : base(dbContextProvider)
        {
        }
    }
}

﻿using Abp.Domain.Repositories;
using Ithinka.Domain.Entities;

namespace Ithinka.EntityFrameworkCore.Repositories
{
    public interface IShiftRepository : IRepository<Shift, int>
    {
    }
}

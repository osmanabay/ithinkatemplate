﻿using Abp.EntityFrameworkCore;
using Ithinka.Domain.Entities;

namespace Ithinka.EntityFrameworkCore.Repositories
{
    public class ShiftRepository : IthinkaRepositoryBase<Shift, int>, IShiftRepository
    {
        public ShiftRepository(IDbContextProvider<IthinkaDbContext> dbContextProvider) 
            : base(dbContextProvider)
        {
        }
    }
}

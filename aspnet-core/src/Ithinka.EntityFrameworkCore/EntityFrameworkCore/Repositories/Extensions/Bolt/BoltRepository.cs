﻿using Abp.EntityFrameworkCore;
using Ithinka.Domain.Entities;

namespace Ithinka.EntityFrameworkCore.Repositories
{
    public class BoltRepository : IthinkaRepositoryBase<Bolt, int>, IBoltRepository
    {
        public BoltRepository(IDbContextProvider<IthinkaDbContext> dbContextProvider) 
            : base(dbContextProvider)
        {
        }
    }
}

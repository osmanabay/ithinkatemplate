﻿using Abp.EntityFrameworkCore;
using Ithinka.Domain.Entities;

namespace Ithinka.EntityFrameworkCore.Repositories
{
    public class TeamEmployeeRepository : IthinkaRepositoryBase<TeamEmployee, int>, ITeamEmployeeRepository
    {
        public TeamEmployeeRepository(IDbContextProvider<IthinkaDbContext> dbContextProvider) 
            : base(dbContextProvider)
        {
        }
    }
}

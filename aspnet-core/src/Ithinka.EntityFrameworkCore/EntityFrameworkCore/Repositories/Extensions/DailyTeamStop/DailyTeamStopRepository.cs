﻿using Abp.EntityFrameworkCore;
using Ithinka.Domain.Entities;

namespace Ithinka.EntityFrameworkCore.Repositories
{
    public class DailyTeamStopRepository : IthinkaRepositoryBase<DailyTeamStop, int>, IDailyTeamStopRepository
    {
        public DailyTeamStopRepository(IDbContextProvider<IthinkaDbContext> dbContextProvider) 
            : base(dbContextProvider)
        {
        }
    }
}

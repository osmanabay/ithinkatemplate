﻿using Abp.EntityFrameworkCore;
using Ithinka.Domain.Entities;

namespace Ithinka.EntityFrameworkCore.Repositories
{
    public class FactoryRepository : IthinkaRepositoryBase<Factory, int>, IFactoryRepository
    {
        public FactoryRepository(IDbContextProvider<IthinkaDbContext> dbContextProvider) 
            : base(dbContextProvider)
        {
        }
    }
}

﻿using Abp.EntityFrameworkCore;
using Ithinka.Domain.Entities;

namespace Ithinka.EntityFrameworkCore.Repositories
{
    public class DailyTeamWorkOrderRepository : IthinkaRepositoryBase<DailyTeamWorkOrder, int>
    {
        public DailyTeamWorkOrderRepository(IDbContextProvider<IthinkaDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }
    }
}

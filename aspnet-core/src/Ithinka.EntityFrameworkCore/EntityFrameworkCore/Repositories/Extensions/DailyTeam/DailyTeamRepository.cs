﻿using Abp.EntityFrameworkCore;
using Ithinka.Domain.Entities;

namespace Ithinka.EntityFrameworkCore.Repositories
{
    public class DailyTeamRepository : IthinkaRepositoryBase<DailyTeam, int>, IDailyTeamRepository
    {
        public DailyTeamRepository(IDbContextProvider<IthinkaDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
    }
}

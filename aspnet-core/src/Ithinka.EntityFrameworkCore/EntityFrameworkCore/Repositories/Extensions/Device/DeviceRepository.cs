﻿using Abp.EntityFrameworkCore;
using Ithinka.Domain.Entities;

namespace Ithinka.EntityFrameworkCore.Repositories
{
    public class DeviceRepository : IthinkaRepositoryBase<Device, int>, IDeviceRepository
    {
        public DeviceRepository(IDbContextProvider<IthinkaDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }
    }
}
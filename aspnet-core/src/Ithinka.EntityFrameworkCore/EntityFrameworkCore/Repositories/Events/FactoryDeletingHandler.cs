﻿using Abp.Dependency;
using Abp.Events.Bus.Entities;
using Abp.Events.Bus.Handlers;
using Ithinka.Domain.Entities;

namespace Ithinka.EntityFrameworkCore.Repositories.Events
{
    public class FactoryDeletingHandler : IEventHandler<EntityDeletingEventData<Factory>>, ITransientDependency
    {
        private readonly IDepartmentRepository _departmentRepository;

        public FactoryDeletingHandler(IDepartmentRepository departmentRepository)
        {
            _departmentRepository = departmentRepository;
        }

        public void HandleEvent(EntityDeletingEventData<Factory> eventData)
        {
            var factory = eventData.Entity;

            foreach (var department in factory.Departments)
            {
                _departmentRepository.Delete(department);
            }
        }
    }
}

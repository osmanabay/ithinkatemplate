﻿using Abp.Dependency;
using Abp.Events.Bus.Entities;
using Abp.Events.Bus.Handlers;
using Ithinka.Domain.Entities;

namespace Ithinka.EntityFrameworkCore.Repositories.Events
{
    public class DepartmentDeletingHandler : IEventHandler<EntityDeletingEventData<Department>>, ITransientDependency
    {
        private readonly IDeviceGroupRepository _deviceGroupRepository;

        public DepartmentDeletingHandler(IDeviceGroupRepository deviceGroupRepository)
        {
            _deviceGroupRepository = deviceGroupRepository;
        }

        public void HandleEvent(EntityDeletingEventData<Department> eventData)
        {
            var department = eventData.Entity;

            foreach (var deviceGroup in department.DeviceGroups)
            {
                _deviceGroupRepository.Delete(deviceGroup);
            }
        }
    }
}

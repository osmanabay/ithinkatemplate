﻿using Abp.Application.Services.Dto;

namespace Ithinka.Authorization.Users.Dtos
{
    public class UserPartOutPut : EntityDto<long>
    {
        public string Name { get; set; }
    }
}

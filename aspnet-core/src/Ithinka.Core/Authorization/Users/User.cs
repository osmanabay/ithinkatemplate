﻿using System;
using System.Collections.Generic;
using Abp.Authorization.Users;
using Abp.Extensions;
using Ithinka.Domain.Entities;

namespace Ithinka.Authorization.Users
{
    public class User : AbpUser<User>
    {

        public virtual ICollection<Team> Teams { get; set; }
        public virtual ICollection<TeamEmployee> TeamEmployees { get; set; }
        public const string DefaultPassword = "123qwe";

        public User()
        {
            Teams = new HashSet<Team>();
            TeamEmployees = new HashSet<TeamEmployee>();
        }

        public static string CreateRandomPassword()
        {
            return Guid.NewGuid().ToString("N").Truncate(16);
        }

        public static User CreateTenantAdminUser(int tenantId, string emailAddress)
        {
            var user = new User
            {
                TenantId = tenantId,
                UserName = AdminUserName,
                Name = AdminUserName,
                Surname = AdminUserName,
                EmailAddress = emailAddress,
                Roles = new List<UserRole>()
            };

            user.SetNormalizedNames();

            return user;
        }
    }
}

﻿using Abp.Authorization;
using Ithinka.Authorization.Roles;
using Ithinka.Authorization.Users;

namespace Ithinka.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}

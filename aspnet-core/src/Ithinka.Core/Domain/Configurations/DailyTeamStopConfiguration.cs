﻿using Ithinka.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ithinka.Domain.Configurations
{
    public class DailyTeamStopConfiguration : IEntityTypeConfiguration<DailyTeamStop>
    {
        public void Configure(EntityTypeBuilder<DailyTeamStop> builder)
        {
            #region Properties
            builder.ToTable("DailyTeamStop");

            builder.HasKey(dailyTeamStop => dailyTeamStop.Id);
            builder.Property(dailyTeamStop => dailyTeamStop.Duration)
                .HasColumnName("Duration");
            builder.Property(dailyTeamStop => dailyTeamStop.StartDate)
                .HasColumnName("StartDate");
            builder.Property(dailyTeamStop => dailyTeamStop.EndDate)
                .HasColumnName("EndDate");
            #endregion Properties

            #region Relations
            #endregion Relations

            #region OptimisticLockField
            builder.Property(dailyTeam => dailyTeam.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}

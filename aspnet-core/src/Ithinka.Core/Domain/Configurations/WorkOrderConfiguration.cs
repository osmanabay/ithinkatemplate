﻿using Ithinka.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ithinka.Domain.Configurations
{
    public class WorkOrderConfiguration : IEntityTypeConfiguration<WorkOrder>
    {
        public void Configure(EntityTypeBuilder<WorkOrder> builder)
        {
            #region Properties
            builder.ToTable("WorkOrder");

            builder.HasKey(workOrder => workOrder.Id);

            //builder.Property(workOrder => workOrder.Name)
            //    .HasColumnName("Name")
            //    .HasMaxLength(100);

            //builder.Property(device => device.DeviceNo)
            //    .HasColumnName("DeviceNo")
            //    .HasMaxLength(50);

            //builder.Property(device => device.IsOpened)
            //    .HasColumnName("IsOpened");
            #endregion Properties

            #region Relations
            builder.HasMany<DailyTeamWorkOrder>(workOrder => workOrder.DailyTeamWorkOrders)
                .WithOne(dailyTeamWorkOrder => dailyTeamWorkOrder.WorkOrder)
                .HasForeignKey(dailyTeamWorkOrder => dailyTeamWorkOrder.WorkOrderId)
                .OnDelete(DeleteBehavior.ClientSetNull);
            #endregion Relations

            #region OptimisticLockField
            builder.Property(deviceGroup => deviceGroup.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}

﻿using Ithinka.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ithinka.Domain.Configurations
{
    public class ShiftConfiguration : IEntityTypeConfiguration<Shift>
    {
        public void Configure(EntityTypeBuilder<Shift> builder)
        {
            #region Properties
            builder.ToTable("Shift");

            builder.HasKey(shift => shift.Id);

            builder.Property(shift => shift.Name)
                .HasColumnName("Name")
                .HasMaxLength(100);
            #endregion Properties

            #region Relations
            builder.HasMany<DailyTeam>(shift => shift.DailyTeams)
              .WithOne(dailyTeam => dailyTeam.Shift)
              .HasForeignKey(dailyTeam => dailyTeam.ShiftId)
              .OnDelete(DeleteBehavior.ClientSetNull);
            #endregion Relations

            #region OptimisticLockField
            builder.Property(factory => factory.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}

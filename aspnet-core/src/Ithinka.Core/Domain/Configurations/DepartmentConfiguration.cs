﻿using Ithinka.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace Ithinka.Domain.Configurations
{
    public class DepartmentConfiguration : IEntityTypeConfiguration<Department>
    {
        public void Configure(EntityTypeBuilder<Department> builder)
        {
            #region Properties
            builder.ToTable("Department");

            builder.HasKey(department => department.Id);

            builder.Property(department => department.Name)
                .HasColumnName("Name")
                .HasMaxLength(150);
            #endregion Properties

            #region Relations
            builder.HasMany<DeviceGroup>(department => department.DeviceGroups)
                .WithOne(deviceGroup => deviceGroup.Department)
                .HasForeignKey(deviceGroup => deviceGroup.DepartmentId)
                .OnDelete(DeleteBehavior.Cascade);
            #endregion Relations

            #region OptimisticLockField
            builder.Property(department => department.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}

﻿using Ithinka.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ithinka.Domain.Configurations
{
    public class FactoryConfiguration : IEntityTypeConfiguration<Factory>
    {
        public void Configure(EntityTypeBuilder<Factory> builder)
        {
            #region Properties
            builder.ToTable("Factory");

            builder.HasKey(factory => factory.Id);

            builder.Property(factory => factory.Name)
                .HasColumnName("Name")
                .HasMaxLength(100);
            #endregion Properties

            #region Relations
            builder.HasMany<Department>(factory => factory.Departments)
                .WithOne(department => department.Factory)
                .HasForeignKey(department => department.FactoryId)
                .OnDelete(DeleteBehavior.Cascade);
            #endregion Relations

            #region OptimisticLockField
            builder.Property(factory => factory.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}

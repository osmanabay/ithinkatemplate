﻿using Ithinka.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ithinka.Domain.Configurations
{
    public class BoltConfiguration : IEntityTypeConfiguration<Bolt>
    {
        public void Configure(EntityTypeBuilder<Bolt> builder)
        {
            #region Properties
            builder.ToTable("Bolt");

            builder.HasKey(bolt => bolt.Id);

            builder.Property(bolt => bolt.TargetAmount)
                .HasColumnName("TargetAmount");

            builder.Property(bolt => bolt.Amount)
                .HasColumnName("Amount");

            builder.Property(bolt => bolt.WeftAmount)
                .HasColumnName("WeftAmount");
            #endregion Properties

            #region Relations
            #endregion Relations

            #region OptimisticLockField
            builder.Property(bolt => bolt.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}

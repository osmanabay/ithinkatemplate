﻿using Ithinka.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ithinka.Domain.Configurations
{
    public class TeamEmployeeConfiguration : IEntityTypeConfiguration<TeamEmployee>
    {
        public void Configure(EntityTypeBuilder<TeamEmployee> builder)
        {
            builder.ToTable("TeamEmployee");

            builder.HasKey(teamEmployee => new { teamEmployee.Id });

            #region Relations
            //builder
            //    .HasOne<DeviceGroup>(dgs => dgs.DeviceGroup)
            //    .WithMany(deviceGroup => deviceGroup.DeviceGroupStops)
            //    .HasForeignKey(dgs => dgs.DeviceGroupId)
            //    .OnDelete(DeleteBehavior.ClientSetNull);

            //builder
            //    .HasOne<Stop>(dgs => dgs.Stop)
            //    .WithMany(stop => stop.DeviceGroupStops)
            //    .HasForeignKey(dgs => dgs.StopId)
            //    .OnDelete(DeleteBehavior.ClientSetNull);
            #endregion Relations
            #region OptimisticLockField
            builder.Property(teamEmployee => teamEmployee.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}

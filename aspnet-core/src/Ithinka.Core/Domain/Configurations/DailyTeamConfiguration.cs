﻿using Ithinka.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ithinka.Domain.Configurations
{
    public class DailyTeamConfiguration : IEntityTypeConfiguration<DailyTeam>
    {
        public void Configure(EntityTypeBuilder<DailyTeam> builder)
        {
            #region Properties
            builder.ToTable("DailyTeam");

            builder.HasKey(dailyTeam => dailyTeam.Id);
            #endregion Properties

            #region Relations
            builder.HasMany<DailyTeamDevice>(dailyTeam => dailyTeam.DailyTeamDevices)
               .WithOne(dailyTeamDevice => dailyTeamDevice.DailyTeam)
               .HasForeignKey(dailyTeamDevice => dailyTeamDevice.DailyTeamId)
               .OnDelete(DeleteBehavior.ClientSetNull);

            builder.HasMany<DailyTeamStop>(dailyTeam => dailyTeam.DailyTeamStops)
               .WithOne(dailyTeamStop => dailyTeamStop.DailyTeam)
               .HasForeignKey(dailyTeamStop => dailyTeamStop.DailyTeamId)
               .OnDelete(DeleteBehavior.ClientSetNull);

            builder.HasMany<DailyTeamWorkOrder>(dailyTeam => dailyTeam.DailyTeamWorkOrders)
               .WithOne(dailyTeamWorkOrder => dailyTeamWorkOrder.DailyTeam)
               .HasForeignKey(dailyTeamWorkOrder => dailyTeamWorkOrder.DailyTeamId)
               .OnDelete(DeleteBehavior.ClientSetNull);
            #endregion Relations

            #region OptimisticLockField
            builder.Property(dailyTeam => dailyTeam.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}

﻿using Ithinka.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ithinka.Domain.Configurations
{
    public class DailyTeamDeviceConfiguration : IEntityTypeConfiguration<DailyTeamDevice>
    {
        public void Configure(EntityTypeBuilder<DailyTeamDevice> builder)
        {
            builder.ToTable("DailyTeamDevice");

            builder.HasKey(dtd => new { dtd.Id });

            #region Relations
            //builder
            //    .HasOne<DeviceGroup>(dgs => dgs.DeviceGroup)
            //    .WithMany(deviceGroup => deviceGroup.DeviceGroupStops)
            //    .HasForeignKey(dgs => dgs.DeviceGroupId)
            //    .OnDelete(DeleteBehavior.ClientSetNull);

            //builder
            //    .HasOne<Stop>(dgs => dgs.Stop)
            //    .WithMany(stop => stop.DeviceGroupStops)
            //    .HasForeignKey(dgs => dgs.StopId)
            //    .OnDelete(DeleteBehavior.ClientSetNull);
            #endregion Relations
            #region OptimisticLockField
            builder.Property(dtd => dtd.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}

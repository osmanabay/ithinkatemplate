﻿using Ithinka.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ithinka.Domain.Configurations
{
    public class DailyTeamWorkOrderConfiguration : IEntityTypeConfiguration<DailyTeamWorkOrder>
    {
        public void Configure(EntityTypeBuilder<DailyTeamWorkOrder> builder)
        {
            #region Properties
            builder.ToTable("DailyTeamWorkOrder");

            builder.HasKey(dailyTeamWorkOrder => dailyTeamWorkOrder.Id);

            builder.Property(dailyTeamWorkOrder => dailyTeamWorkOrder.TargetAmount)
                .HasColumnName("TargetAmount");

            builder.Property(dailyTeamWorkOrder => dailyTeamWorkOrder.Amount)
                .HasColumnName("Amount");

            builder.Property(dailyTeamWorkOrder => dailyTeamWorkOrder.WeftAmount)
                .HasColumnName("WeftAmount");

            builder.Property(dailyTeamWorkOrder => dailyTeamWorkOrder.StartDate)
                .HasColumnName("StartDate");

            builder.Property(dailyTeamWorkOrder => dailyTeamWorkOrder.EndDate)
                .HasColumnName("EndDate");

            builder.Property(dailyTeamWorkOrder => dailyTeamWorkOrder.StartupTime)
               .HasColumnName("StartupTime");

            builder.Property(dailyTeamWorkOrder => dailyTeamWorkOrder.TotalTimeout)
               .HasColumnName("TotalTimeout");
            #endregion Properties

            #region Relations
            builder.HasMany<Bolt>(dailyTeamWorkOrder => dailyTeamWorkOrder.Bolts)
                .WithOne(bolt => bolt.DailyTeamWorkOrder)
                .HasForeignKey(bolt => bolt.DailyTeamWorkOrderId)
                .OnDelete(DeleteBehavior.ClientSetNull);
            #endregion Relations

            #region OptimisticLockField
            builder.Property(dailyTeamWorkOrder => dailyTeamWorkOrder.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}

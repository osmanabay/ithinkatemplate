﻿using Ithinka.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ithinka.Domain.Configurations
{
    public class StopConfiguration : IEntityTypeConfiguration<Stop>
    {
        public void Configure(EntityTypeBuilder<Stop> builder)
        {
            #region Properties
            builder.ToTable("Stop");

            builder.HasKey(stop => stop.Id);

            builder.Property(stop => stop.Name)
                .HasColumnName("Name")
                .HasMaxLength(150);

            builder.Property(stop => stop.Duration)
                .HasColumnName("Duration");
            #endregion Properties

            #region Relations
            builder.HasMany<DeviceGroupStop>(stop => stop.DeviceGroupStops)
             .WithOne(deviceGroupStop => deviceGroupStop.Stop)
             .HasForeignKey(deviceGroupStop => deviceGroupStop.StopId)
             .OnDelete(DeleteBehavior.ClientSetNull);

            builder.HasMany<DailyTeamStop>(stop => stop.DailyTeamStops)
             .WithOne(dailyTeamStop => dailyTeamStop.Stop)
             .HasForeignKey(dailyTeamStop => dailyTeamStop.StopId)
             .OnDelete(DeleteBehavior.ClientSetNull);
            #endregion Relations

            #region OptimisticLockField
            builder.Property(stop => stop.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}

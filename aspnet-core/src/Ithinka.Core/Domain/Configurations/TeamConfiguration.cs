﻿using Ithinka.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ithinka.Domain.Configurations
{
    public class TeamConfiguration : IEntityTypeConfiguration<Team>
    {
        public void Configure(EntityTypeBuilder<Team> builder)
        {
            #region Properties
            builder.ToTable("Team");

            builder.HasKey(team => team.Id);

            builder.Property(team => team.Name)
                .HasColumnName("Name")
                .HasMaxLength(100);
            #endregion Properties

            #region Relations
            builder.HasMany<DailyTeam>(team => team.DailyTeams)
              .WithOne(dailyTeam => dailyTeam.Team)
              .HasForeignKey(dailyTeam => dailyTeam.TeamId)
              .OnDelete(DeleteBehavior.ClientSetNull);

            builder.HasMany<TeamEmployee>(team => team.TeamEmployees)
              .WithOne(teamEmployee => teamEmployee.Team)
              .HasForeignKey(teamEmployee => teamEmployee.TeamId)
              .OnDelete(DeleteBehavior.ClientSetNull);
            #endregion Relations

            #region OptimisticLockField
            builder.Property(factory => factory.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}

﻿using Abp.Domain.Entities.Auditing;
using Ithinka.Authorization.Users;

namespace Ithinka.Domain.Entities
{
    public class TeamEmployee : FullAuditedEntity<int>
    {
        #region Constructor
        public TeamEmployee()
        {

        }
        #endregion Constructor

        #region Properties
        #endregion Properties

        #region Relations
        #region One To One Relations

        #endregion One To One Relations

        #region One To Many Relations
        public int? TeamId { get; set; }
        public virtual Team Team { get; set; }
        public long? EmployeeId { get; set; }
        public virtual User Employee { get; set; }
        #endregion One To Many Relations

        #region Many To Many Relations

        #endregion Many To Many Relations
        #endregion Relations

        #region OptimisticLockField
        public byte[] RowVersion { get; set; }
        #endregion OptimisticLockField
    }
}

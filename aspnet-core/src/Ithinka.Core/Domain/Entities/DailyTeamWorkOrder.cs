﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;

namespace Ithinka.Domain.Entities
{
    public class DailyTeamWorkOrder : FullAuditedEntity<int>
    {
        #region Constructor
        public DailyTeamWorkOrder()
        {
            Bolts = new HashSet<Bolt>();
        }
        #endregion Constructor

        #region Properties
        public decimal TargetAmount { get; set; }
        public decimal Amount { get; set; }
        public decimal WeftAmount { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int StartupTime { get; set; }
        public int TotalTimeout { get; set; }
        #endregion Properties

        #region Relations
        #region One To One Relations

        #endregion One To One Relations

        #region One To Many Relations
        public int? DailyTeamId { get; set; }
        public virtual DailyTeam DailyTeam { get; set; }
        public int? WorkOrderId { get; set; }
        public virtual WorkOrder WorkOrder { get; set; }

        public virtual ICollection<Bolt> Bolts { get; set; }
        #endregion One To Many Relations

        #region Many To Many Relations

        #endregion Many To Many Relations
        #endregion Relations

        #region OptimisticLockField
        public byte[] RowVersion { get; set; }
        #endregion OptimisticLockField
    }
}

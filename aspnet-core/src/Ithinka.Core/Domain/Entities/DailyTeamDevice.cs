﻿using Abp.Domain.Entities.Auditing;

namespace Ithinka.Domain.Entities
{
    public class DailyTeamDevice : FullAuditedEntity<int>
    {
        #region Constructor
        public DailyTeamDevice()
        {

        }
        #endregion Constructor

        #region Properties

        #endregion Properties

        #region Relations
        #region One To One Relations

        #endregion One To One Relations

        #region One To Many Relations
        public int? DeviceId { get; set; }
        public virtual Device Device { get; set; }

        public int? DailyTeamId { get; set; }
        public virtual DailyTeam DailyTeam { get; set; }
        #endregion One To Many Relations

        #region Many To Many Relations

        #endregion Many To Many Relations
        #endregion Relations

        #region OptimisticLockField
        public byte[] RowVersion { get; set; }
        #endregion OptimisticLockField
    }
}

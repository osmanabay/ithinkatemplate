﻿using Abp.Domain.Entities.Auditing;
using System;

namespace Ithinka.Domain.Entities
{
    public class DailyTeamStop : FullAuditedEntity<int>
    {
        #region Constructor
        public DailyTeamStop()
        {

        }
        #endregion Constructor

        #region Properties
        public int Duration { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        #endregion Properties

        #region Relations
        #region One To One Relations

        #endregion One To One Relations

        #region One To Many Relations
        public int? DailyTeamId { get; set; }
        public virtual DailyTeam DailyTeam { get; set; }
        public int? StopId { get; set; }
        public virtual Stop Stop { get; set; }
        #endregion One To Many Relations

        #region Many To Many Relations

        #endregion Many To Many Relations
        #endregion Relations

        #region OptimisticLockField
        public byte[] RowVersion { get; set; }
        #endregion OptimisticLockField
    }
}

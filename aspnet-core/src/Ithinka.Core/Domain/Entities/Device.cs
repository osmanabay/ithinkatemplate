﻿using Abp.Domain.Entities.Auditing;
using System.Collections.Generic;

namespace Ithinka.Domain.Entities
{
    public class Device : FullAuditedEntity<int>
    {
        #region Constructor
        public Device()
        {
            DailyTeamDevices = new HashSet<DailyTeamDevice>();
            WorkOrders = new HashSet<WorkOrder>();
        }
        #endregion Constructor

        #region Properties
        public string Name { get; set; }
        public string DeviceNo { get; set; }
        public bool IsOpened { get; set; }
        #endregion Properties

        #region Relations
        #region One To One Relations

        #endregion One To One Relations

        #region One To Many Relations
        public int? DeviceGroupId { get; set; }
        public virtual DeviceGroup DeviceGroup { get; set; }

        public virtual ICollection<DailyTeamDevice> DailyTeamDevices { get; set; }
        public virtual ICollection<WorkOrder> WorkOrders { get; set; }
        #endregion One To Many Relations

        #region Many To Many Relations

        #endregion Many To Many Relations
        #endregion Relations

        #region OptimisticLockField
        public byte[] RowVersion { get; set; }
        #endregion OptimisticLockField
    }
}

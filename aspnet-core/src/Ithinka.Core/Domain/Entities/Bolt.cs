﻿using Abp.Domain.Entities.Auditing;

namespace Ithinka.Domain.Entities
{
    public class Bolt : FullAuditedEntity<int>
    {
        #region Constructor
        public Bolt()
        {

        }
        #endregion Constructor

        #region Properties
        public decimal Amount { get; set; }
        public decimal TargetAmount { get; set; }
        public int WeftAmount { get; set; }
        #endregion Properties

        #region Relations
        #region One To One Relations

        #endregion One To One Relations

        #region One To Many Relations
        public int? DailyTeamWorkOrderId { get; set; }
        public virtual DailyTeamWorkOrder DailyTeamWorkOrder { get; set; }
        #endregion One To Many Relations

        #region Many To Many Relations

        #endregion Many To Many Relations
        #endregion Relations

        #region OptimisticLockField
        public byte[] RowVersion { get; set; }
        #endregion OptimisticLockField
    }
}

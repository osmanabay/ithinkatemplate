﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;

namespace Ithinka.Domain.Entities
{
    public class Shift : FullAuditedEntity<int>
    {

        #region Constructor
        public Shift()
        {
            DailyTeams = new HashSet<DailyTeam>();
        }
        #endregion Constructor

        #region Properties
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        #endregion Properties

        #region Relations
        #region One To One Relations

        #endregion One To One Relations

        #region One To Many Relations
        public virtual ICollection<DailyTeam> DailyTeams { get; set; }
        #endregion One To Many Relations

        #region Many To Many Relations

        #endregion Many To Many Relations
        #endregion Relations

        #region OptimisticLockField
        public byte[] RowVersion { get; set; }
        #endregion OptimisticLockField
    }
}

﻿using Abp.Domain.Entities.Auditing;
using Ithinka.Authorization.Users;
using System.Collections.Generic;

namespace Ithinka.Domain.Entities
{
    public class Team : FullAuditedEntity<int>
    {
        #region Constructor
        public Team()
        {
            DailyTeams = new HashSet<DailyTeam>();
            TeamEmployees = new HashSet<TeamEmployee>();
        }
        #endregion Constructor

        #region Properties
        public string Name { get; set; }
        #endregion Properties

        #region Relations
        #region One To One Relations

        #endregion One To One Relations

        #region One To Many Relations
        public long? LeadEmployeeId { get; set; }
        public virtual User LeadEmployee { get; set; }

        public virtual ICollection<DailyTeam> DailyTeams { get; set; }
        public virtual ICollection<TeamEmployee> TeamEmployees { get; set; }
        #endregion One To Many Relations

        #region Many To Many Relations

        #endregion Many To Many Relations
        #endregion Relations

        #region OptimisticLockField
        public byte[] RowVersion { get; set; }
        #endregion OptimisticLockField
    }
}

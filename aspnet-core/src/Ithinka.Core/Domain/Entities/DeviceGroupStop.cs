﻿using Abp.Domain.Entities.Auditing;
using System;

namespace Ithinka.Domain.Entities
{
    public class DeviceGroupStop : FullAuditedEntity<int>
    {
        #region Constructor
        public DeviceGroupStop()
        {

        }
        #endregion Constructor

        #region Properties
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        #endregion Properties

        #region Relations
        #region One To One Relations
        public int? DeviceGroupId { get; set; }
        public virtual DeviceGroup DeviceGroup { get; set; }
        public int? StopId { get; set; }
        public virtual Stop Stop { get; set; }
        #endregion One To One Relations

        #region One To Many Relations

        #endregion One To Many Relations

        #region Many To Many Relations

        #endregion Many To Many Relations
        #endregion Relations

        #region OptimisticLockField
        public byte[] RowVersion { get; set; }
        #endregion OptimisticLockField
    }
}

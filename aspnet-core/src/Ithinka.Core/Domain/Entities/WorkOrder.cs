﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;

namespace Ithinka.Domain.Entities
{
    public class WorkOrder : FullAuditedEntity<int>
    {
        #region Constructor
        public WorkOrder()
        {
            DailyTeamWorkOrders = new HashSet<DailyTeamWorkOrder>();
        }
        #endregion Constructor

        #region Properties
        public string ProductId { get; set; }
        public string ProductName { get; set; }
        public string No { get; set; }
        public bool? IsOpened { get; set; }
        public decimal? TargetAmount { get; set; }
        public decimal? Amount { get; set; }
        public string WarpNo { get; set; }
        public decimal WeftDensity { get; set; }

        public decimal? WeftAmount { get; set; }
        public bool Sample { get; set; } = false;
        public int Status { get; set; } = 0;
        public bool ProductionContinue { get; set; } = false;
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        #endregion Properties

        #region Relations
        #region One To One Relations

        #endregion One To One Relations

        #region One To Many Relations
        public int? DeviceId { get; set; }
        public virtual Device Device { get; set; }
        public virtual ICollection<DailyTeamWorkOrder> DailyTeamWorkOrders { get; set; }
        #endregion One To Many Relations

        #region Many To Many Relations

        #endregion Many To Many Relations
        #endregion Relations

        #region OptimisticLockField
        public byte[] RowVersion { get; set; }
        #endregion OptimisticLockField
    }
}

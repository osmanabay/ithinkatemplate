﻿using Abp.Domain.Entities.Auditing;
using System.Collections.Generic;

namespace Ithinka.Domain.Entities
{
    public class DailyTeam : FullAuditedEntity<int>
    {
        #region Constructor
        public DailyTeam()
        {
            DailyTeamDevices = new HashSet<DailyTeamDevice>();
            DailyTeamStops = new HashSet<DailyTeamStop>();
            DailyTeamWorkOrders = new HashSet<DailyTeamWorkOrder>();
        }
        #endregion Constructor

        #region Properties
        #endregion Properties

        #region Relations
        #region One To One Relations

        #endregion One To One Relations

        #region One To Many Relations
        public int? TeamId { get; set; }
        public virtual Team Team { get; set; }

        public int? ShiftId { get; set; }
        public virtual Shift Shift { get; set; }

        public virtual ICollection<DailyTeamDevice> DailyTeamDevices { get; set; }
        public virtual ICollection<DailyTeamStop> DailyTeamStops { get; set; }
        public virtual ICollection<DailyTeamWorkOrder> DailyTeamWorkOrders { get; set; }
        #endregion One To Many Relations

        #region Many To Many Relations

        #endregion Many To Many Relations
        #endregion Relations

        #region OptimisticLockField
        public byte[] RowVersion { get; set; }
        #endregion OptimisticLockField
    }
}

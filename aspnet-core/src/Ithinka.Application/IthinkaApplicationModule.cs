﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Ithinka.Authorization;
using Ithinka.Manager;

namespace Ithinka
{
    [DependsOn(
        typeof(IthinkaCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class IthinkaApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<IthinkaAuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(IthinkaApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(cfg =>
            {
                MapperManager.DtosToDomain(cfg);

                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg.AddMaps(thisAssembly);
            });
        }
    }
}

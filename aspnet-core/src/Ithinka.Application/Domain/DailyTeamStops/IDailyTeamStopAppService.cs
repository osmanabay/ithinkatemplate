﻿using Abp.Application.Services;
using Ithinka.Domain.DailyTeamStops.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ithinka.Domain.DailyTeamStops
{
    public interface IDailyTeamStopAppService : IApplicationService
    {
        #region Async
        Task<DailyTeamStopFullOutPut> CreateAsync(CreateDailyTeamStopInput input);
        Task<DailyTeamStopFullOutPut> GetAsync(GetDailyTeamStopInput input);
        Task<List<DailyTeamStopFullOutPut>> GetListAsync();
        Task DeleteAsync(DeleteDailyTeamStopInput input);
        Task<DailyTeamStopFullOutPut> UpdateAsync(UpdateDailyTeamStopInput input);
        #endregion
    }
}

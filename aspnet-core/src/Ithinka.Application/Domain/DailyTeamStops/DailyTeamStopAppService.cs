﻿using Abp.Domain.Entities;
using Abp.Domain.Uow;
using Ithinka.Domain.DailyTeamStops.Dtos;
using Ithinka.Domain.Entities;
using Ithinka.EntityFrameworkCore.Repositories;
using Ithinka.Manager;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ithinka.Domain.DailyTeamStops
{
    public class DailyTeamStopAppService : IthinkaAppServiceBase, IDailyTeamStopAppService
    {
        private readonly IEntityManager _entityManager;
        private readonly IDailyTeamStopRepository _dailyTeamStopRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public DailyTeamStopAppService(
            IEntityManager entityManager,
            IDailyTeamStopRepository dailyTeamStopRepository ,
            IUnitOfWorkManager unitOfWorkManager)
        {
            _entityManager = entityManager;
            _dailyTeamStopRepository = dailyTeamStopRepository;
            _unitOfWorkManager = unitOfWorkManager;
        }

        public async Task<DailyTeamStopFullOutPut> CreateAsync(CreateDailyTeamStopInput input)
        {
            var creatingDailyTeamStop = new DailyTeamStop()
            {
                StartDate = DateTime.Now,
                DailyTeamId = input.DailyTeam.Id,
                StopId = input.Stop.Id
            };

            await _dailyTeamStopRepository.InsertAsync(creatingDailyTeamStop);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            var createdDailyTeamStop = ObjectMapper.Map<DailyTeamStopFullOutPut>(creatingDailyTeamStop);
            createdDailyTeamStop.DailyTeam = input.DailyTeam;
            createdDailyTeamStop.Stop = input.Stop;

            return createdDailyTeamStop;
        }
        public async Task<DailyTeamStopFullOutPut> GetAsync(GetDailyTeamStopInput input)
        {
            var dailyTeamStop = await _entityManager.GetDailyTeamStopAsync(input.Id);
            return ObjectMapper.Map<DailyTeamStopFullOutPut>(dailyTeamStop);
        }
        public async Task<List<DailyTeamStopFullOutPut>> GetListAsync()
        {
            try
            {
                var dailyTeamStops = await _dailyTeamStopRepository.GetAllListAsync();
                return ObjectMapper.Map<List<DailyTeamStopFullOutPut>>(dailyTeamStops);
            }
            catch (Exception ex)
            {
                throw new EntityNotFoundException(ex.Message);
            }
        }
        public async Task DeleteAsync(DeleteDailyTeamStopInput input)
        {
            var dailyTeamStop = await _entityManager.GetDailyTeamStopAsync(input.Id);
            await _dailyTeamStopRepository.DeleteAsync(dailyTeamStop);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }
        public async Task<DailyTeamStopFullOutPut> UpdateAsync(UpdateDailyTeamStopInput input)
        {
            var dailyTeamStop = await _entityManager.GetDailyTeamStopAsync(input.Id);
            dailyTeamStop.EndDate = input.EndDate.HasValue ? DateTime.Now : dailyTeamStop.EndDate;
            dailyTeamStop.Duration = input.EndDate.HasValue ?
                (int)((dailyTeamStop.EndDate - dailyTeamStop.StartDate).TotalSeconds) 
                : dailyTeamStop.Duration;
            dailyTeamStop.StopId = input.Stop == null ? dailyTeamStop.StopId : input.Stop.Id;

            await _dailyTeamStopRepository.UpdateAsync(dailyTeamStop);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<DailyTeamStopFullOutPut>(dailyTeamStop);
        }
    }
}

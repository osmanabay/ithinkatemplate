﻿using Ithinka.Domain.DailyTeams.Dtos;
using Ithinka.Domain.Stops.Dtos;
using System;

namespace Ithinka.Domain.DailyTeamStops.Dtos
{
    public class CreateDailyTeamStopInput
    {
        public DailyTeamPartOutPut DailyTeam { get; set; }
        public StopPartOutPut Stop { get; set; }
    }
}

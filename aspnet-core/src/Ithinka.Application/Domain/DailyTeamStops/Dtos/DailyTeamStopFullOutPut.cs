﻿using Abp.Application.Services.Dto;
using Ithinka.Domain.DailyTeams.Dtos;
using Ithinka.Domain.Stops.Dtos;
using System;

namespace Ithinka.Domain.DailyTeamStops.Dtos
{
    public class DailyTeamStopFullOutPut : EntityDto<int>
    {
        public int? Duration { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public DailyTeamPartOutPut DailyTeam { get; set; }
        public StopPartOutPut Stop { get; set; }
    }
}

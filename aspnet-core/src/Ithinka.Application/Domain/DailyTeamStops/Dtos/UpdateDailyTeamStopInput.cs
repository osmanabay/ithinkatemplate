﻿using Abp.Application.Services.Dto;
using Ithinka.Domain.DailyTeams.Dtos;
using Ithinka.Domain.Stops.Dtos;
using System;

namespace Ithinka.Domain.DailyTeamStops.Dtos
{
    public class UpdateDailyTeamStopInput : EntityDto<int>
    {
        public DateTime? EndDate { get; set; }
        public StopPartOutPut Stop { get; set; }
    }
}

﻿using Abp.Application.Services.Dto;
using Ithinka.Domain.Factories.Dtos;

namespace Ithinka.Domain.Departments.Dtos
{
    public class UpdateDepartmentInput: EntityDto<int>
    {
        public string Name { get; set; }
        public FactoryPartOutPut Factory { get; set; }
    }
}

﻿using Ithinka.Domain.Factories.Dtos;

namespace Ithinka.Domain.Departments.Dtos
{
    public class CreateDepartmentInput
    {
        public string Name { get; set; }
        public FactoryPartOutPut Factory { get; set; }
    }
}

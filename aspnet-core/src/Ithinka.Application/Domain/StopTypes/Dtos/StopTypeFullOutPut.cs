﻿using Abp.Application.Services.Dto;

namespace Ithinka.Domain.StopTypes.Dtos
{
    public class StopTypeFullOutPut : EntityDto<int>
    {
        public string Name { get; set; }
    }
}

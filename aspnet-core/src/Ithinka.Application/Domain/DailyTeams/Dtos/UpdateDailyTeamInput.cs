﻿using Abp.Application.Services.Dto;
using Ithinka.Domain.Shifts.Dtos;
using Ithinka.Domain.Teams.Dtos;

namespace Ithinka.Domain.DailyTeams.Dtos
{
    public class UpdateDailyTeamInput : EntityDto<int>
    {
        public ShiftPartOutPut Shift { get; set; }
        public TeamPartOutPut Team { get; set; }
    }
}

﻿using Ithinka.Domain.Shifts.Dtos;
using Ithinka.Domain.Teams.Dtos;
using System.ComponentModel.DataAnnotations;

namespace Ithinka.Domain.DailyTeams.Dtos
{
    public class CreateDailyTeamInput 
    {
        [Required]
        public ShiftPartOutPut Shift { get; set; }
        public TeamPartOutPut Team { get; set; }
    }
}

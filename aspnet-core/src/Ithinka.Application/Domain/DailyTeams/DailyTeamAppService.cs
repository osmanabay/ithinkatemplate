﻿using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.Domain.Uow;
using Ithinka.Authorization;
using Ithinka.Domain.DailyTeams.Dtos;
using Ithinka.Domain.Entities;
using Ithinka.EntityFrameworkCore.Repositories;
using Ithinka.Manager;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ithinka.Domain.DailyTeams
{
    [AbpAuthorize(PermissionNames.DailyTeam)]
    public class DailyTeamAppService : IthinkaAppServiceBase, IDailyTeamAppService
    {
        private readonly IEntityManager _entityManager;
        private readonly IDailyTeamRepository _dailyTeamRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public DailyTeamAppService(
            IEntityManager entityManager,
            IDailyTeamRepository dailyTeamRepository,
            IUnitOfWorkManager unitOfWorkManager)
        {
            _entityManager = entityManager;
            _dailyTeamRepository = dailyTeamRepository;
            _unitOfWorkManager = unitOfWorkManager;
        }

        [HttpPost]
        [AbpAuthorize(PermissionNames.DailyTeam_Create)]
        public async Task<DailyTeamFullOutPut> CreateAsync(CreateDailyTeamInput input)
        {
            var creatingDailyTeam = new DailyTeam()
            {
                ShiftId = input.Shift?.Id,
                TeamId = input.Team?.Id
            };

            await _dailyTeamRepository.InsertAsync(creatingDailyTeam);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            var createdDailyTeam = ObjectMapper.Map<DailyTeamFullOutPut>(creatingDailyTeam);
            createdDailyTeam.Shift = input.Shift;
            createdDailyTeam.Team = input.Team;

            return createdDailyTeam;
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.DailyTeam_Get)]
        public async Task<DailyTeamFullOutPut> GetAsync(GetDailyTeamInput input)
        {
            var dailyTeam = await _entityManager.GetDailyTeamAsync(input.Id);
            return ObjectMapper.Map<DailyTeamFullOutPut>(dailyTeam);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.DailyTeam_GetList)]
        public async Task<List<DailyTeamFullOutPut>> GetListAsync()
        {
            try
            {
                var dailyTeams = await _dailyTeamRepository.GetAllListAsync();
                return ObjectMapper.Map<List<DailyTeamFullOutPut>>(dailyTeams);
            }
            catch (Exception ex)
            {
                throw new EntityNotFoundException(ex.Message);
            }
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.DailyTeam_Delete)]
        public async Task DeleteAsync(DeleteDailyTeamInput input)
        {
            var deletingDailyTeam = await _entityManager.GetDailyTeamAsync(input.Id);
            await _dailyTeamRepository.DeleteAsync(deletingDailyTeam);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.DailyTeam_Update)]
        public async Task<DailyTeamFullOutPut> UpdateAsync(UpdateDailyTeamInput input)
        {
            var updatingDailyTeam = await _entityManager.GetDailyTeamAsync(input.Id);
            updatingDailyTeam.ShiftId = input.Shift == null ? updatingDailyTeam.ShiftId : input.Shift.Id;
            updatingDailyTeam.TeamId = input.Team == null ? updatingDailyTeam.TeamId : input.Team.Id;

            await _dailyTeamRepository.UpdateAsync(updatingDailyTeam);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            var updatedDailyTeam = ObjectMapper.Map<DailyTeamFullOutPut>(updatingDailyTeam);

            return updatedDailyTeam;
        }
    }
}

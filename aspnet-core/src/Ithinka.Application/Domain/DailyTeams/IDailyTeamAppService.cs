﻿using Abp.Application.Services;
using Ithinka.Domain.DailyTeams.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ithinka.Domain.DailyTeams
{
    public interface IDailyTeamAppService : IApplicationService
    {
        #region Async
        Task<DailyTeamFullOutPut> CreateAsync(CreateDailyTeamInput input);
        Task<DailyTeamFullOutPut> GetAsync(GetDailyTeamInput input);
        Task<List<DailyTeamFullOutPut>> GetListAsync();
        Task DeleteAsync(DeleteDailyTeamInput input);
        Task<DailyTeamFullOutPut> UpdateAsync(UpdateDailyTeamInput input);
        #endregion
    }
}

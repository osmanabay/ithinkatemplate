﻿using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.Domain.Uow;
using Abp.Extensions;
using Ithinka.Authorization;
using Ithinka.Domain.Entities;
using Ithinka.Domain.Shifts.Dtos;
using Ithinka.EntityFrameworkCore.Repositories;
using Ithinka.Manager;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ithinka.Domain.Shifts
{
    [AbpAuthorize(PermissionNames.Shift)]
    public class ShiftAppService : IthinkaAppServiceBase, IShiftAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IShiftRepository _shiftRepository;
        private readonly IEntityManager _entityManager;

        public ShiftAppService(IUnitOfWorkManager unitOfWorkManager,
            IShiftRepository shiftRepository,
            IEntityManager entityManager)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _shiftRepository = shiftRepository;
            _entityManager = entityManager;
        }

        [HttpPost]
        [AbpAuthorize(PermissionNames.Shift_Create)]
        public async Task<ShiftFullOutPut> CreateAsync(CreateShiftInput input)
        {
            var createdShift = new Shift()
            {
                Name = input.Name,
                StartDate = input.StartDate,
                EndDate = input.EndDate
            };

            await _shiftRepository.InsertAsync(createdShift);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<ShiftFullOutPut>(createdShift);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Shift_Get)]
        public async Task<ShiftFullOutPut> GetAsync(GetShiftInput input)
        {
            var shif = await _entityManager.GetShiftAsync(input.Id);
            return ObjectMapper.Map<ShiftFullOutPut>(shif);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Shift_GetList)]
        public async Task<List<ShiftFullOutPut>> GetListAsync()
        {
            try
            {
                var shifts = await _shiftRepository.GetAllListAsync();
                return ObjectMapper.Map<List<ShiftFullOutPut>>(shifts);
            }
            catch (Exception ex)
            {

                throw new EntityNotFoundException(ex.Message);
            }
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Shift_Delete)]
        public async Task DeleteAsync(DeleteShiftInput input)
        {
            var shif = await _entityManager.GetShiftAsync(input.Id);
            await _shiftRepository.DeleteAsync(shif);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Shift_Update)]
        public async Task<ShiftFullOutPut> UpdateAsync(UpdateShiftInput input)
        {
            var shif = await _entityManager.GetShiftAsync(input.Id);

            shif.Name = input.Name.IsNullOrEmpty() ? shif.Name : input.Name;
            shif.StartDate = input.StartDate;
            shif.EndDate = input.EndDate;

            return ObjectMapper.Map<ShiftFullOutPut>(shif);
        }
    }
}

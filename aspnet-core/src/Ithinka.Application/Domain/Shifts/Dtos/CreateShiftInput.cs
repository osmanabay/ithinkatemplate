﻿using System;

namespace Ithinka.Domain.Shifts.Dtos
{
    public class CreateShiftInput
    {
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}

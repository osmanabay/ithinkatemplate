﻿using Abp.Application.Services.Dto;

namespace Ithinka.Domain.Shifts.Dtos
{
    public class ShiftPartOutPut : EntityDto<int>
    {
        public string Name { get; set; }
        //public DateTime StartDate { get; set; }
        //public DateTime EndDate { get; set; }
    }
}

﻿using Abp.Application.Services.Dto;
using System;

namespace Ithinka.Domain.Shifts.Dtos
{
    public class ShiftFullOutPut : EntityDto<int>
    {
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}

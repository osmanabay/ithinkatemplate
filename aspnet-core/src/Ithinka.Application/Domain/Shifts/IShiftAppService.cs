﻿using Abp.Application.Services;
using Ithinka.Domain.Shifts.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ithinka.Domain.Shifts
{
    public interface IShiftAppService : IApplicationService
    {
        #region Async
        Task<ShiftFullOutPut> CreateAsync(CreateShiftInput input);
        Task<ShiftFullOutPut> GetAsync(GetShiftInput input);
        Task<List<ShiftFullOutPut>> GetListAsync();
        Task DeleteAsync(DeleteShiftInput input);
        Task<ShiftFullOutPut> UpdateAsync(UpdateShiftInput input);
        #endregion
    }
}

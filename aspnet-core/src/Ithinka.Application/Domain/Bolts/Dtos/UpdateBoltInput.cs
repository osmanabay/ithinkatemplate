﻿using Abp.Application.Services.Dto;
using Ithinka.Domain.DailyTeamWorkOrders.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ithinka.Domain.Bolts.Dtos
{
    public class UpdateBoltInput : EntityDto<int>
    {
        public decimal? Amount { get; set; }
        public decimal? TargetAmount { get; set; }
        public int? WeftAmount { get; set; }
        public DailyTeamWorkOrderPartOutPut DailyTeamWorkOrder { get; set; }
    }
}

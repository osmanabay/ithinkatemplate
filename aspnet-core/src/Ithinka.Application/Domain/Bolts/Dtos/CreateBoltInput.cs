﻿using Ithinka.Domain.DailyTeamWorkOrders.Dtos;

namespace Ithinka.Domain.Bolts.Dtos
{
    public class CreateBoltInput
    {
        public decimal Amount { get; set; }
        public decimal TargetAmount { get; set; }
        public int WeftAmount { get; set; }
        public DailyTeamWorkOrderPartOutPut DailyTeamWorkOrder { get; set; }
    }
}

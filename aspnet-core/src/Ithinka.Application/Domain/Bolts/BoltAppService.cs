﻿using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.Domain.Uow;
using Ithinka.Authorization;
using Ithinka.Domain.Bolts.Dtos;
using Ithinka.Domain.Entities;
using Ithinka.EntityFrameworkCore.Repositories;
using Ithinka.Manager;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ithinka.Domain.Bolts
{
    [AbpAuthorize(PermissionNames.Bolt)]
    public class BoltAppService : IthinkaAppServiceBase, IBoltAppService
    {
        private readonly IEntityManager _entityManager;
        private readonly IBoltRepository _boltRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public BoltAppService(
            IEntityManager entityManager,
            IBoltRepository boltRepository,
            IUnitOfWorkManager unitOfWorkManager)
        {
            _entityManager = entityManager;
            _boltRepository = boltRepository;
            _unitOfWorkManager = unitOfWorkManager;
        }

        [HttpPost]
        [AbpAuthorize(PermissionNames.Bolt_Create)]
        public async Task<BoltFullOutPut> CreateAsync(CreateBoltInput input)
        {
            var createdBolt = new Bolt()
            {
                Amount = input.Amount,
                WeftAmount = input.WeftAmount,
                TargetAmount = input.TargetAmount,
                DailyTeamWorkOrderId = input.DailyTeamWorkOrder?.Id
            };

            await _boltRepository.InsertAsync(createdBolt);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<BoltFullOutPut>(createdBolt);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Bolt_Get)]
        public async Task<BoltFullOutPut> GetAsync(GetBoltInput input)
        {
            var bolt = await _entityManager.GetBoltAsync(input.Id);
            return ObjectMapper.Map<BoltFullOutPut>(bolt);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Bolt_GetList)]
        public async Task<List<BoltFullOutPut>> GetListAsync()
        {
            try
            {
                var bolts = await _boltRepository.GetAllListAsync();
                return ObjectMapper.Map<List<BoltFullOutPut>>(bolts);
            }
            catch (Exception ex)
            {
                throw new EntityNotFoundException(ex.Message);
            }
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Bolt_GetList)]
        public async Task<List<BoltFullOutPut>> GetListPartialAsync(GetBoltListInput input)
        {
            try
            {
                var bolts = await _boltRepository.GetAllListAsync(b => 
                    b.DailyTeamWorkOrderId == input.DailyTeamWorkOrderId);
                return ObjectMapper.Map<List<BoltFullOutPut>>(bolts);
            }
            catch (Exception ex)
            {
                throw new EntityNotFoundException(ex.Message);
            }
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Bolt_Delete)]
        public async Task DeleteAsync(DeleteBoltInput input)
        {
            var bolt = await _entityManager.GetBoltAsync(input.Id);
            await _boltRepository.DeleteAsync(bolt);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Bolt_Update)]
        public async Task<BoltFullOutPut> UpdateAsync(UpdateBoltInput input)
        {
            var bolt = await _entityManager.GetBoltAsync(input.Id);

            bolt.Amount = input.Amount.HasValue ? input.Amount.Value : bolt.Amount;
            bolt.WeftAmount = input.WeftAmount.HasValue ? input.WeftAmount.Value : bolt.WeftAmount;
            bolt.TargetAmount = input.TargetAmount.HasValue ? input.TargetAmount.Value : bolt.TargetAmount;
            bolt.DailyTeamWorkOrderId = input.DailyTeamWorkOrder?.Id;

            await _boltRepository.UpdateAsync(bolt);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<BoltFullOutPut>(bolt);
        }
    }
}

﻿using Abp.Application.Services;
using Ithinka.Domain.Bolts.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ithinka.Domain.Bolts
{
    public interface IBoltAppService : IApplicationService
    {
        #region Async
        Task<BoltFullOutPut> CreateAsync(CreateBoltInput input);
        Task<BoltFullOutPut> GetAsync(GetBoltInput input);
        Task<List<BoltFullOutPut>> GetListAsync();
        Task<List<BoltFullOutPut>> GetListPartialAsync(GetBoltListInput input);
        Task DeleteAsync(DeleteBoltInput input);
        Task<BoltFullOutPut> UpdateAsync(UpdateBoltInput input);
        #endregion
    }
}

﻿using Abp.Application.Services.Dto;

namespace Ithinka.Domain.DeviceGroups.Dtos
{
    public class DeviceGroupPartOutPut : EntityDto<int>
    {
        public string Name { get; set; }
        public string IPAddress { get; set; }
    }
}

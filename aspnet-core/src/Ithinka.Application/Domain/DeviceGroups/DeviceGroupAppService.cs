﻿using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.Domain.Uow;
using Abp.Extensions;
using Ithinka.Authorization;
using Ithinka.Domain.DeviceGroups.Dtos;
using Ithinka.Domain.Entities;
using Ithinka.EntityFrameworkCore.Repositories;
using Ithinka.Manager;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ithinka.Domain.DeviceGroups
{
    public class DeviceGroupAppService : IthinkaAppServiceBase, IDeviceGroupAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IEntityManager _entityManager;
        private readonly IDeviceGroupRepository _deviceGroupRepository;

        public DeviceGroupAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IEntityManager entityManager,
            IDeviceGroupRepository deviceGroupRepository)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _entityManager = entityManager;
            _deviceGroupRepository = deviceGroupRepository;
        }

        [HttpPost]
        [AbpAuthorize(PermissionNames.DeviceGroup_Create)]
        public async Task<DeviceGroupFullOutPut> CreateAsync(CreateDeviceGroupInput input)
        {
            var deviceGroup = new DeviceGroup()
            {
                Name = input.Name,
                IPAddress = input.IPAddress,
                DepartmentId = input.Department?.Id
            };

            await _deviceGroupRepository.InsertAsync(deviceGroup);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<DeviceGroupFullOutPut>(deviceGroup);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.DeviceGroup_Get)]
        public async Task<DeviceGroupFullOutPut> GetAsync(GetDeviceGroupInput input)
        {
            var deviceGroup = await _entityManager.GetDeviceGroupAsync(input.Id);

            return ObjectMapper.Map<DeviceGroupFullOutPut>(deviceGroup);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.DeviceGroup_GetList)]
        public async Task<List<DeviceGroupFullOutPut>> GetListAsync()
        {
            try
            {
                var deviceGroups = await _deviceGroupRepository.GetAllListAsync();

                return ObjectMapper.Map<List<DeviceGroupFullOutPut>>(deviceGroups);
            }
            catch (Exception ex)
            {

                throw new EntityNotFoundException(typeof(DeviceGroup),ex);
            }
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.DeviceGroup_Delete)]
        public async Task DeleteAsync(DeleteDeviceGroupInput input)
        {
            var deviceGroup = await _entityManager.GetDeviceGroupAsync(input.Id);

            await _deviceGroupRepository.DeleteAsync(deviceGroup.Id);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.DeviceGroup_Update)]
        public async Task<DeviceGroupFullOutPut> UpdateAsync(UpdateDeviceGroupInput input)
        {
            var deviceGroup = await _entityManager.GetDeviceGroupAsync(input.Id);

            deviceGroup.Name = input.Name.IsNullOrEmpty() ? deviceGroup.Name : input.Name;
            deviceGroup.IPAddress = input.IPAddress.IsNullOrEmpty() ? deviceGroup.IPAddress : 
                input.IPAddress;
            deviceGroup.DepartmentId = input.Department?.Id;

            await _deviceGroupRepository.UpdateAsync(deviceGroup);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<DeviceGroupFullOutPut>(deviceGroup);
        }
    }
}

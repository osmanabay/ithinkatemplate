﻿using Abp.Application.Services;
using Ithinka.Domain.DeviceGroups.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ithinka.Domain.DeviceGroups
{
    public interface IDeviceGroupAppService : IApplicationService
    {
        #region Async Methods
        Task<DeviceGroupFullOutPut> CreateAsync(CreateDeviceGroupInput input);
        Task<DeviceGroupFullOutPut> GetAsync(GetDeviceGroupInput input);
        Task<List<DeviceGroupFullOutPut>> GetListAsync();
        Task DeleteAsync(DeleteDeviceGroupInput input);
        Task<DeviceGroupFullOutPut> UpdateAsync(UpdateDeviceGroupInput input);
        #endregion Async Methods
    }
}

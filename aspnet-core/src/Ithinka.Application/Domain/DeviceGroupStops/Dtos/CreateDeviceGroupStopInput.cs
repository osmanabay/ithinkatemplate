﻿using Ithinka.Domain.DeviceGroups.Dtos;
using Ithinka.Domain.Stops.Dtos;
using System;
using System.ComponentModel.DataAnnotations;

namespace Ithinka.Domain.DeviceGroupStops.Dtos
{
    public class CreateDeviceGroupStopInput
    {
        [Required]
        public DateTime StartDate { get; set; }
        [Required]
        public DateTime EndDate { get; set; }

        [Required]
        public DeviceGroupPartOutPut DeviceGroup { get; set; }
        [Required]
        public StopFullOutPut Stop { get; set; }
    }
}

﻿using Abp.Application.Services.Dto;
using Ithinka.Domain.DeviceGroups.Dtos;
using Ithinka.Domain.Stops.Dtos;

namespace Ithinka.Domain.DeviceGroupStops.Dtos
{
    public class DeviceGroupStopFullOutPut : EntityDto<int>
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }

        public DeviceGroupFullOutPut DeviceGroup { get; set; }
        public StopFullOutPut Stop { get; set; }
    }
}

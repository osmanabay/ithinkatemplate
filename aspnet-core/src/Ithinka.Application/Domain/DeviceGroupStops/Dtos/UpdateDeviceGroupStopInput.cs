﻿using Abp.Application.Services.Dto;
using Ithinka.Domain.DeviceGroups.Dtos;
using Ithinka.Domain.Stops.Dtos;
using System;

namespace Ithinka.Domain.DeviceGroupStops.Dtos
{
    public class UpdateDeviceGroupStopInput : EntityDto<int>
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public DeviceGroupPartOutPut DeviceGroup { get; set; }
        public StopFullOutPut Stop { get; set; }
    }
}

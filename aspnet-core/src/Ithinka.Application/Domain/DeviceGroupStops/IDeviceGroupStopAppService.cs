﻿using Abp.Application.Services;
using Ithinka.Domain.DeviceGroupStops.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ithinka.Domain.DeviceGroupStops
{
    public interface IDeviceGroupStopAppService : IApplicationService
    {
        #region Async Method
        Task<DeviceGroupStopFullOutPut> CreateAsync(CreateDeviceGroupStopInput input);
        Task<DeviceGroupStopFullOutPut> GetAsync(GetDeviceGroupStopInput input);
        Task<List<DeviceGroupStopFullOutPut>> GetListAsync();
        Task DeleteAsync(DeleteDeviceGroupStopInput input);
        Task<DeviceGroupStopFullOutPut> UpdateAsync(UpdateDeviceGroupStopInput input);
        #endregion Async Method
    }
}

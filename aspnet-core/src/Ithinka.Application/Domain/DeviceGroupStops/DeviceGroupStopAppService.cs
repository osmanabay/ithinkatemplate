﻿using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.Domain.Uow;
using Ithinka.Authorization;
using Ithinka.Domain.DeviceGroupStops.Dtos;
using Ithinka.Domain.Entities;
using Ithinka.EntityFrameworkCore.Repositories.Extensions.DeviceGropStop;
using Ithinka.Manager;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ithinka.Domain.DeviceGroupStops
{
    [AbpAuthorize(PermissionNames.DeviceGroupStop)]
    public class DeviceGroupStopAppService : IthinkaAppServiceBase, IDeviceGroupStopAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IEntityManager _entityManager;
        private readonly IDeviceGroupStopRepository  _deviceGroupStopRepository;

        public DeviceGroupStopAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IEntityManager entityManager,
            IDeviceGroupStopRepository deviceGroupStopRepository)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _entityManager = entityManager;
            _deviceGroupStopRepository = deviceGroupStopRepository;
        }

        [HttpPost]
        [AbpAuthorize(PermissionNames.DeviceGroupStop_Create)]
        public async Task<DeviceGroupStopFullOutPut> CreateAsync(CreateDeviceGroupStopInput input)
        {
            try
            {
                var deviceGroupStop = new DeviceGroupStop()
                {
                    StartDate = input.StartDate,
                    EndDate = input.EndDate,
                    DeviceGroupId = input.DeviceGroup.Id,
                    StopId = input.Stop.Id
                };

                await _deviceGroupStopRepository.InsertAsync(deviceGroupStop);
                await _unitOfWorkManager.Current.SaveChangesAsync();
                var ss = ObjectMapper.Map<DeviceGroupStopFullOutPut>(deviceGroupStop); 
                return ObjectMapper.Map<DeviceGroupStopFullOutPut>(deviceGroupStop);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
            
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.DeviceGroupStop_Get)]
        public async Task<DeviceGroupStopFullOutPut> GetAsync(GetDeviceGroupStopInput input)
        {
            var deviceGroupStop = await _entityManager.GetDeviceGroupStopAsync(input.Id);

            return ObjectMapper.Map<DeviceGroupStopFullOutPut>(deviceGroupStop);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.DeviceGroupStop_GetList)]
        public async Task<List<DeviceGroupStopFullOutPut>> GetListAsync()
        {
            try
            {
                var deviceGroupStos = await _deviceGroupStopRepository.GetAllListAsync();
                var returned = ObjectMapper.Map<List<DeviceGroupStopFullOutPut>>(deviceGroupStos);
                return returned;
            }
            catch (Exception ex)
            {
                throw new EntityNotFoundException(ex.Message);
            }
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.DeviceGroupStop_Delete)]
        public async Task DeleteAsync(DeleteDeviceGroupStopInput input)
        {
            var deviceGroupStop = await _entityManager.GetDeviceGroupStopAsync(input.Id);

            await _deviceGroupStopRepository.DeleteAsync(deviceGroupStop);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.DeviceGroupStop_Update)]
        public async Task<DeviceGroupStopFullOutPut> UpdateAsync(UpdateDeviceGroupStopInput input)
        {
            var deviceGroupStop = await _entityManager.GetDeviceGroupStopAsync(input.Id);

            deviceGroupStop.StartDate = input.StartDate;
            deviceGroupStop.EndDate = input.EndDate;
            deviceGroupStop.DeviceGroupId = null;
            deviceGroupStop.DeviceGroupId = input.DeviceGroup.Id;

            deviceGroupStop.StopId = input.Stop.Id;

            await _deviceGroupStopRepository.UpdateAsync(deviceGroupStop);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<DeviceGroupStopFullOutPut>(deviceGroupStop);
        }
    }
}

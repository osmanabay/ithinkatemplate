﻿using Abp.Application.Services.Dto;

namespace Ithinka.Domain.Factories.Dtos
{
    public class FactoryPartOutPut: EntityDto<int>
    {
        public string Name { get; set; }
    }
}

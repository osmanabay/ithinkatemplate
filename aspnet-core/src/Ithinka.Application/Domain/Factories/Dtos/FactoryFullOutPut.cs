﻿using Abp.Application.Services.Dto;
using Ithinka.Domain.Departments.Dtos;
using System.Collections.Generic;

namespace Ithinka.Domain.Factories.Dtos
{
    public class FactoryFullOutPut : EntityDto<int>
    {
        public string Name { get; set; }

        // Factory nesnesini geri dönerken ona ait Departmanları da geri dönmek isterseniz 
        // bu kodu ektid ediniz
        //public List<DepartmentPartOutPut> DepartmentParts { get; set; }
    }
}

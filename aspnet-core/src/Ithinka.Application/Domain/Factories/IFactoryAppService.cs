﻿using Abp.Application.Services;
using DevExtreme.AspNet.Data.ResponseModel;
using Ithinka.Devexpress;
using Ithinka.Domain.Factories.Dtos;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ithinka.Domain.Factories
{
    public interface IFactoryAppService : IApplicationService
    {
        #region Async Methods
        Task<FactoryFullOutPut> CreateAsync(CreateFactoryInput input);
        Task<FactoryFullOutPut> GetAsync(GetFactoryInput input);
        Task<JsonResult> GetListAsync(DataSourceLoadOptions loadOptions);
        Task DeleteAsync(DeleteFactoryInput input);
        Task<FactoryFullOutPut> UpdateAsync(UpdateFactoryInput input);
        #endregion Async Methods
    }
}

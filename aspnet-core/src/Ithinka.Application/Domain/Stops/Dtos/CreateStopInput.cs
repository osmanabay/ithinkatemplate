﻿using Ithinka.Domain.StopTypes.Dtos;

namespace Ithinka.Domain.Stops.Dtos
{
    public class CreateStopInput
    {
        public string Name { get; set; }
        public int? Duration { get; set; }

        public StopTypeFullOutPut StopType { get; set; }
    }
}

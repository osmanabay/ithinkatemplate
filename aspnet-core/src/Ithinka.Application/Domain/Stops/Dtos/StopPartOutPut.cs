﻿using Abp.Application.Services.Dto;

namespace Ithinka.Domain.Stops.Dtos
{
    public class StopPartOutPut : EntityDto<int>
    {
        public string Name { get; set; }
        public int? Duration { get; set; }
    }
}

﻿using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.Domain.Uow;
using Abp.Extensions;
using Ithinka.Authorization;
using Ithinka.Domain.Entities;
using Ithinka.Domain.Stops.Dtos;
using Ithinka.EntityFrameworkCore.Repositories;
using Ithinka.Manager;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ithinka.Domain.Stops
{
    public class StopAppService : IthinkaAppServiceBase, IStopAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IEntityManager _entityManager;
        private readonly IStopRepository _stopRepository;

        public StopAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IEntityManager entityManager,
            IStopRepository stopRepository)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _entityManager = entityManager;
            _stopRepository = stopRepository;
        }

        [HttpPost]
        [AbpAuthorize(PermissionNames.Stop_Create)]
        public async Task<StopFullOutPut> CreateAsync(CreateStopInput input)
        {
            var stop = new Stop()
            {
                Name = input.Name,
                Duration = input.Duration.Value,
                StopTypeId = input.StopType?.Id
            };

            await _stopRepository.InsertAsync(stop);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<StopFullOutPut>(stop);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Stop_Get)]
        public async Task<StopFullOutPut> GetAsync(GetStopInput input)
        {
            var stop = await _entityManager.GetStopAsync(input.Id);

            return ObjectMapper.Map<StopFullOutPut>(stop);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Stop_GetList)]
        public async Task<List<StopFullOutPut>> GetListAsync()
        {
            try
            {
                var stops = await _stopRepository.GetAllListAsync();
                return ObjectMapper.Map<List<StopFullOutPut>>(stops);
            }
            catch (Exception ex)
            {
                throw new EntityNotFoundException(typeof(Stop), ex);
            }
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Stop_Delete)]
        public async Task DeleteAsync(DeleteStopInput input)
        {
            var stop = await _entityManager.GetStopAsync(input.Id);

            await _stopRepository.DeleteAsync(stop.Id);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Stop_Update)]
        public async Task<StopFullOutPut> UpdateAsync(UpdateStopInput input)
        {
            var stop = await _entityManager.GetStopAsync(input.Id);

            stop.Name = input.Name.IsNullOrEmpty() ? stop.Name : input.Name;
            stop.Duration = input.Duration.HasValue ? input.Duration.Value : stop.Duration;
            stop.StopTypeId = input.StopType?.Id;

            await _stopRepository.UpdateAsync(stop);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<StopFullOutPut>(stop);
        }
    }
}

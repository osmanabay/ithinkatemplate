﻿using Abp.Application.Services.Dto;

namespace Ithinka.Domain.Teams.Dtos
{
    public class TeamPartOutPut : EntityDto<int>
    {
        public string Name { get; set; }
    }
}

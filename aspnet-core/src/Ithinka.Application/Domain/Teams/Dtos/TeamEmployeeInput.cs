﻿using Abp.Application.Services.Dto;
using Ithinka.Authorization.Users.Dtos;

namespace Ithinka.Domain.Teams.Dtos
{
    public class TeamEmployeeInput : EntityDto<int>
    {
        public UserPartOutPut Employee { get; set; }
    }
}

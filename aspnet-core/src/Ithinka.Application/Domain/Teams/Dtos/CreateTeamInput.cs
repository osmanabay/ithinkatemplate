﻿using Ithinka.Authorization.Users.Dtos;

namespace Ithinka.Domain.Teams.Dtos
{
    public class CreateTeamInput
    {
        public string Name { get; set; }
        public UserPartOutPut LeadEmployee { get; set; }
    }
}

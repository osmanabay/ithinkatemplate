﻿using Abp.Application.Services.Dto;
using Ithinka.Authorization.Users.Dtos;

namespace Ithinka.Domain.Teams.Dtos
{
    public class TeamFullOutPut : EntityDto<int>
    {
        public string Name { get; set; }
        public UserPartOutPut LeadEmployee { get; set; }
    }
}

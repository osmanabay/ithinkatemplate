﻿using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.UI;
using Ithinka.Authorization;
using Ithinka.Domain.Entities;
using Ithinka.Domain.Teams.Dtos;
using Ithinka.EntityFrameworkCore.Repositories;
using Ithinka.Manager;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ithinka.Domain.Teams
{
    [AbpAuthorize(PermissionNames.Team)]
    public class TeamAppService : IthinkaAppServiceBase, ITeamAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IEntityManager _entityManager;
        private readonly ITeamRepository _teamRepository;
        private readonly ITeamEmployeeRepository _teamEmployeeRepository;

        public TeamAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IEntityManager entityManager,
            ITeamRepository teamRepository,
            ITeamEmployeeRepository teamEmployeeRepository)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _entityManager = entityManager;
            _teamRepository = teamRepository;
            _teamEmployeeRepository = teamEmployeeRepository;
        }

        [HttpPost]
        [AbpAuthorize(PermissionNames.Team_Create)]
        public async Task<TeamFullOutPut> CreateAsync(CreateTeamInput input)
        {
            var createdTeam = new Team()
            {
                Name = input.Name,
                LeadEmployeeId = input.LeadEmployee?.Id
            };

            await _teamRepository.InsertAsync(createdTeam);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<TeamFullOutPut>(createdTeam);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Team_GetList)]
        public async Task<TeamFullOutPut> GetAsync(GetTeamInput input)
        {
            var team = await _entityManager.GetTeamAsync(input.Id);
            return ObjectMapper.Map<TeamFullOutPut>(team);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Team_GetList)]
        public async Task<List<TeamFullOutPut>> GetListAsync()
        {
            try
            {
                var teams = await _teamRepository.GetAllListAsync();
                return ObjectMapper.Map<List<TeamFullOutPut>>(teams);
            }
            catch (Exception ex)
            {
                throw new EntityNotFoundException(ex.Message);
            }
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Team_Delete)]
        public async Task DeleteAsync(DeleteTeamInput input)
        {
            var team = await _entityManager.GetTeamAsync(input.Id);
            await _teamRepository.DeleteAsync(team);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Team_Update)]
        public async Task<TeamFullOutPut> UpdateAsync(UpdateTeamInput input)
        {
            var updatedTeam = await _entityManager.GetTeamAsync(input.Id);

            updatedTeam.Name = input.Name.IsNullOrEmpty() ? updatedTeam.Name : input.Name;
            updatedTeam.LeadEmployeeId = input.LeadEmployee?.Id;

            return ObjectMapper.Map<TeamFullOutPut>(updatedTeam);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Team_AddEmployee)]
        public async Task AddEmployeeAsync(TeamEmployeeInput input)
        {
            var team = await _entityManager.GetTeamAsync(input.Id);

            team.TeamEmployees.Add(new TeamEmployee()
            {
                EmployeeId = input.Employee?.Id,
                TeamId = input.Id
            });

            await _teamRepository.UpdateAsync(team);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Team_RemoveEmployee)]
        public async Task RemoveEmployeeAsync(TeamEmployeeInput input)
        {
            var teamEmployee = await _teamEmployeeRepository.FirstOrDefaultAsync(te => 
                te.TeamId == input.Id && te.EmployeeId == input.Employee.Id);

            if (teamEmployee == null)
            {
                // Localization dan gelmeli
                throw new UserFriendlyException("Takım Çalışanı bulunamadı.");
            }

            await _teamEmployeeRepository.DeleteAsync(teamEmployee);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }
    }
}

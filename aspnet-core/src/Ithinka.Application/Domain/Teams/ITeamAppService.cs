﻿using Abp.Application.Services;
using Ithinka.Domain.Teams.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ithinka.Domain.Teams
{
    public interface ITeamAppService : IApplicationService
    {
        #region Async
        Task<TeamFullOutPut> CreateAsync(CreateTeamInput input);
        Task<TeamFullOutPut> GetAsync(GetTeamInput input);
        Task<List<TeamFullOutPut>> GetListAsync();
        Task DeleteAsync(DeleteTeamInput input);
        Task<TeamFullOutPut> UpdateAsync(UpdateTeamInput input);
        Task AddEmployeeAsync(TeamEmployeeInput input);
        Task RemoveEmployeeAsync(TeamEmployeeInput input);
        #endregion
    }
}

﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Ithinka.Authorization.Accounts.Dto;

namespace Ithinka.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}

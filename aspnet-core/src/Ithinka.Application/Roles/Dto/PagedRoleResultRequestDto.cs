﻿using Abp.Application.Services.Dto;

namespace Ithinka.Roles.Dto
{
    public class PagedRoleResultRequestDto : PagedResultRequestDto
    {
        public string Keyword { get; set; }
    }
}


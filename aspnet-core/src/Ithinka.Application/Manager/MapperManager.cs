﻿using Ithinka.Domain.Departments.Dtos;
using Ithinka.Domain.Entities;
using Ithinka.Domain.Factories.Dtos;
using AutoMapper;
using Ithinka.Domain.Devices.Dtos;
using Ithinka.Domain.DeviceGroups.Dtos;
using Ithinka.Domain.Stops.Dtos;
using Ithinka.Domain.StopTypes.Dtos;
using Ithinka.Domain.DeviceGroupStops.Dtos;
using Ithinka.Domain.Shifts.Dtos;
using Ithinka.Domain.Teams.Dtos;
using Ithinka.Authorization.Users;
using Ithinka.Authorization.Users.Dtos;
using Ithinka.Domain.Bolts.Dtos;
using Ithinka.Domain.DailyTeams.Dtos;
using Ithinka.Domain.DailyTeamStops.Dtos;

namespace Ithinka.Manager
{
    public class MapperManager
    {
        public static void DtosToDomain(IMapperConfigurationExpression cfg)
        {
            // Bolts
            cfg.CreateMap<Bolt, CreateBoltInput>();
            cfg.CreateMap<Bolt, GetBoltInput>();
            cfg.CreateMap<Bolt, DeleteBoltInput>();
            cfg.CreateMap<Bolt, UpdateBoltInput>();
            cfg.CreateMap<Bolt, BoltFullOutPut>();

            // DailyTeams
            cfg.CreateMap<DailyTeam, CreateDailyTeamInput>();
            cfg.CreateMap<DailyTeam, GetDailyTeamInput>();
            cfg.CreateMap<DailyTeam, DeleteDailyTeamInput>();
            cfg.CreateMap<DailyTeam, UpdateDailyTeamInput>();
            cfg.CreateMap<DailyTeam, DailyTeamFullOutPut>();
            cfg.CreateMap<DailyTeam, DailyTeamPartOutPut>();

            // DailyTeamStops
            cfg.CreateMap<DailyTeamStop, CreateDailyTeamStopInput>();
            cfg.CreateMap<DailyTeamStop, GetDailyTeamStopInput>();
            cfg.CreateMap<DailyTeamStop, DeleteDailyTeamStopInput>();
            cfg.CreateMap<DailyTeamStop, UpdateDailyTeamStopInput>();
            cfg.CreateMap<DailyTeamStop, DailyTeamStopFullOutPut>();
            //cfg.CreateMap<DailyTeamStop, DailyTeamStopPartOutPut>();

            // Departments
            cfg.CreateMap<Department, CreateDepartmentInput>();
            cfg.CreateMap<Department, GetDepartmentInput>();
            cfg.CreateMap<Department, DeleteDepartmentInput>();
            cfg.CreateMap<Department, UpdateDepartmentInput>();
            cfg.CreateMap<Department, DepartmentFullOutPut>();
            cfg.CreateMap<Department, DepartmentPartOutPut>();

            // Factories
            cfg.CreateMap<Factory, CreateFactoryInput>();
            cfg.CreateMap<Factory, GetFactoryInput>();
            cfg.CreateMap<Factory, DeleteFactoryInput>();
            cfg.CreateMap<Factory, UpdateFactoryInput>();
            cfg.CreateMap<Factory, FactoryFullOutPut>();
            cfg.CreateMap<Factory, FactoryPartOutPut>();

            // Devices
            cfg.CreateMap<Device, CreateDeviceInput>();
            cfg.CreateMap<Device, GetDeviceInput>();
            cfg.CreateMap<Device, DeleteDeviceInput>();
            cfg.CreateMap<Device, UpdateDeviceInput>();
            cfg.CreateMap<Device, DeviceFullOutPut>();

            // DeviceGroups
            cfg.CreateMap<DeviceGroup, CreateDeviceGroupInput>();
            cfg.CreateMap<DeviceGroup, GetDeviceGroupInput>();
            cfg.CreateMap<DeviceGroup, DeleteDeviceGroupInput>();
            cfg.CreateMap<DeviceGroup, UpdateDeviceGroupInput>();
            cfg.CreateMap<DeviceGroup, DeviceGroupFullOutPut>();

            // DeviceGroupStops
            cfg.CreateMap<DeviceGroupStop, CreateDeviceGroupStopInput>();
            cfg.CreateMap<DeviceGroupStop, GetDeviceGroupStopInput>();
            cfg.CreateMap<DeviceGroupStop, DeleteDeviceGroupStopInput>();
            cfg.CreateMap<DeviceGroupStop, UpdateDeviceGroupStopInput>();
            cfg.CreateMap<DeviceGroupStop, DeviceGroupStopFullOutPut>();

            // Stops
            cfg.CreateMap<Stop, CreateStopInput>();
            cfg.CreateMap<Stop, GetStopInput>();
            cfg.CreateMap<Stop, DeleteStopInput>();
            cfg.CreateMap<Stop, UpdateStopInput>();
            cfg.CreateMap<Stop, StopFullOutPut>();
            cfg.CreateMap<Stop, StopPartOutPut>();

            // StopTypes
            cfg.CreateMap<StopType, CreateStopTypeInput>();
            cfg.CreateMap<StopType, GetStopTypeInput>();
            cfg.CreateMap<StopType, DeleteStopTypeInput>();
            cfg.CreateMap<StopType, UpdateStopTypeInput>();
            cfg.CreateMap<StopType, StopTypeFullOutPut>();

            // Shits
            cfg.CreateMap<Shift, CreateShiftInput>();
            cfg.CreateMap<Shift, GetShiftInput>();
            cfg.CreateMap<Shift, DeleteShiftInput>();
            cfg.CreateMap<Shift, UpdateShiftInput>();
            cfg.CreateMap<Shift, ShiftFullOutPut>();
            cfg.CreateMap<Shift, ShiftPartOutPut>();
            cfg.CreateMap<ShiftPartOutPut, Shift>();

            // Teams
            cfg.CreateMap<Team, CreateTeamInput>();
            cfg.CreateMap<Team, GetTeamInput>();
            cfg.CreateMap<Team, DeleteTeamInput>();
            cfg.CreateMap<Team, UpdateTeamInput>();
            cfg.CreateMap<Team, TeamFullOutPut>();
            cfg.CreateMap<Team, TeamPartOutPut>();
            cfg.CreateMap<TeamPartOutPut, Team>();

            // Users
            cfg.CreateMap<User, UserPartOutPut>();
            cfg.CreateMap<UserPartOutPut, User>();
        }
    }
}

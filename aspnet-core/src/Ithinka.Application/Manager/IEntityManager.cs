﻿using Abp.Dependency;
using Ithinka.Domain.Entities;
using System.Threading.Tasks;

namespace Ithinka.Manager
{
    public interface IEntityManager : ITransientDependency
    {
        Task<Bolt> GetBoltAsync(int boltId);
        Task<Department> GetDepartmentAsync(int departmentId);
        Task<Factory> GetFactoryAsync(int factoryId);
        Task<Device> GetDeviceAsync(int deviceId);
        Task<DeviceGroup> GetDeviceGroupAsync(int deviceGroupId);
        Task<Stop> GetStopAsync(int stopId);
        Task<StopType> GetStopTypeAsync(int stopTypeId);
        Task<DeviceGroupStop> GetDeviceGroupStopAsync(int deviceGroupStopId);
        Task<Shift> GetShiftAsync(int shiftId);
        Task<Team> GetTeamAsync(int teamId);
        Task<DailyTeam> GetDailyTeamAsync(int dailyTeamId);
        Task<DailyTeamStop> GetDailyTeamStopAsync(int dailyTeamStopId);
    }
}

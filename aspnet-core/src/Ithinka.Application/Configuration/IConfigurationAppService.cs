﻿using System.Threading.Tasks;
using Ithinka.Configuration.Dto;

namespace Ithinka.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}

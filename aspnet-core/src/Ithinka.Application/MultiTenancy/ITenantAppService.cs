﻿using Abp.Application.Services;
using Ithinka.MultiTenancy.Dto;

namespace Ithinka.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}


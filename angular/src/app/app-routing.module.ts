import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AppRouteGuard } from '@shared/auth/auth-route-guard';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { UsersComponent } from './users/users.component';
import { TenantsComponent } from './tenants/tenants.component';
import { RolesComponent } from 'app/roles/roles.component';
import { ChangePasswordComponent } from './users/change-password/change-password.component';
import { DepartmentComponent } from './components/department/department.component';
import { FactoryComponent } from './components/factory/factory.component';
import { DeviceGroupComponent } from './components/device-group/device-group.component';
import { DeviceComponent } from './components/device/device.component';
import { StopTypeComponent } from './components/stop-type/stop-type.component';
import { StopComponent } from './components/stop/stop.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: AppComponent,
                children: [
                    { path: 'home', component: HomeComponent,  canActivate: [AppRouteGuard] },
                    { path: 'users', component: UsersComponent, data: { permission: 'Pages.Users' }, canActivate: [AppRouteGuard] },
                    { path: 'roles', component: RolesComponent, data: { permission: 'Pages.Roles' }, canActivate: [AppRouteGuard] },
                    { path: 'tenants', component: TenantsComponent, data: { permission: 'Pages.Tenants' }, canActivate: [AppRouteGuard] },
                    { path: 'about', component: AboutComponent },
                    { path: 'departments', component: DepartmentComponent, canActivate: [AppRouteGuard]},
                    { path: 'factories', component: FactoryComponent, canActivate: [AppRouteGuard]},
                    { path: 'devicegroups', component: DeviceGroupComponent, canActivate: [AppRouteGuard]},
                    { path: 'devices', component: DeviceComponent, canActivate: [AppRouteGuard]},
                    { path: 'stoptypes', component: StopTypeComponent, canActivate: [AppRouteGuard]},
                    { path: 'stops', component: StopComponent, canActivate: [AppRouteGuard]},
                    { path: 'update-password', component: ChangePasswordComponent }
                ]
            }
        ])
    ],
    exports: [RouterModule]
})
export class AppRoutingModule { }

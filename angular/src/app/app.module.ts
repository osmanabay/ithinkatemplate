import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientJsonpModule } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';

import { ModalModule } from 'ngx-bootstrap';
import { NgxPaginationModule } from 'ngx-pagination';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AbpModule } from '@abp/abp.module';

import { ServiceProxyModule } from '@shared/service-proxies/service-proxy.module';
import { SharedModule } from '@shared/shared.module';
import { IthinkaServiceModule } from './services/ithinka-service.module';

import { HomeComponent } from '@app/home/home.component';
import { AboutComponent } from '@app/about/about.component';
import { TopBarComponent } from '@app/layout/topbar.component';
import { TopBarLanguageSwitchComponent } from '@app/layout/topbar-languageswitch.component';
import { SideBarUserAreaComponent } from '@app/layout/sidebar-user-area.component';
import { SideBarNavComponent } from '@app/layout/sidebar-nav.component';
import { SideBarFooterComponent } from '@app/layout/sidebar-footer.component';
import { RightSideBarComponent } from '@app/layout/right-sidebar.component';
// tenants
import { TenantsComponent } from '@app/tenants/tenants.component';
import { CreateTenantDialogComponent } from './tenants/create-tenant/create-tenant-dialog.component';
import { EditTenantDialogComponent } from './tenants/edit-tenant/edit-tenant-dialog.component';
// roles
import { RolesComponent } from '@app/roles/roles.component';
import { CreateRoleDialogComponent } from './roles/create-role/create-role-dialog.component';
import { EditRoleDialogComponent } from './roles/edit-role/edit-role-dialog.component';
// users
import { UsersComponent } from '@app/users/users.component';
import { CreateUserDialogComponent } from '@app/users/create-user/create-user-dialog.component';
import { EditUserDialogComponent } from '@app/users/edit-user/edit-user-dialog.component';
import { ChangePasswordComponent } from './users/change-password/change-password.component';
import { ResetPasswordDialogComponent } from './users/reset-password/reset-password.component';

import { DepartmentComponent } from './components/department/department.component';
import { CreateDepartmentDialogComponent } from './components/department/create-department/create-department.component';
import { EditDepartmentDialogComponent } from './components/department/edit-department/edit-department.component';

import { FactoryComponent } from './components/factory/factory.component';
import { CreateFactoryDialogComponent } from './components/factory/create-factory/create-factory-dialog.component';
import { EditFactoryDialogComponent } from './components/factory/edit-factory/edit-factory-dialog.component';
import { DevextremeComponentModule } from './devextreme-component.module';
import { DeviceGroupComponent } from './components/device-group/device-group.component';
import { EditDeviceGroupComponent } from './components/device-group/edit-device-group/edit-device-group.component';
import { CreateDeviceGroupComponent } from './components/device-group/create-device-group/create-device-group.component';
import { DeviceGroupsPipe } from './components/device-group/device-group.pipe';
import { DepartmentPipe } from './components/department/department.pipe';
import { DeviceComponent } from './components/device/device.component';
import { StopTypeComponent } from './components/stop-type/stop-type.component';
import { StopComponent } from './components/stop/stop.component';
import { CreateDeviceComponent } from './components/device/create-device/create-device.component';
import { EditDeviceComponent } from './components/device/edit-device/edit-device.component';
import { CreateStopTypeComponent } from './components/stop-type/create-stop-type/create-stop-type.component';
import { EditStopTypeComponent } from './components/stop-type/edit-stop-type/edit-stop-type.component';
import { CreateStopComponent } from './components/stop/create-stop/create-stop.component';
import { EditStopComponent } from './components/stop/edit-stop/edit-stop.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    TopBarComponent,
    TopBarLanguageSwitchComponent,
    SideBarUserAreaComponent,
    SideBarNavComponent,
    SideBarFooterComponent,
    RightSideBarComponent,
    // tenants
    TenantsComponent,
    CreateTenantDialogComponent,
    EditTenantDialogComponent,
    // roles
    RolesComponent,
    CreateRoleDialogComponent,
    EditRoleDialogComponent,
    // users
    UsersComponent,
    CreateUserDialogComponent,
    EditUserDialogComponent,
    ChangePasswordComponent,
    ResetPasswordDialogComponent,
    // Departments
    DepartmentComponent,
    CreateDepartmentDialogComponent,
    EditDepartmentDialogComponent,
    DepartmentPipe,
    // Factories
    FactoryComponent,
    CreateFactoryDialogComponent,
    EditFactoryDialogComponent,
    // DeviceGroups
    DeviceGroupComponent,
    CreateDeviceGroupComponent,
    EditDeviceGroupComponent,
    DeviceGroupsPipe,
    // Devices
    DeviceComponent,
    CreateDeviceComponent,
    EditDeviceComponent,
    // StopTypes
    StopTypeComponent,
    CreateStopTypeComponent,
    EditStopTypeComponent,
    // Stops
    StopComponent,
    CreateStopComponent,
    EditStopComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    HttpClientJsonpModule,
    ModalModule.forRoot(),
    AbpModule,
    AppRoutingModule,
    ServiceProxyModule,
    SharedModule,
    NgxPaginationModule,
    IthinkaServiceModule,
    DevextremeComponentModule
  ],
  providers: [],
  entryComponents: [
    // tenants
    CreateTenantDialogComponent,
    EditTenantDialogComponent,
    // roles
    CreateRoleDialogComponent,
    EditRoleDialogComponent,
    // users
    CreateUserDialogComponent,
    EditUserDialogComponent,
    ResetPasswordDialogComponent,
    // departments
    CreateDepartmentDialogComponent,
    EditDepartmentDialogComponent,
    // Factories
    CreateFactoryDialogComponent,
    EditFactoryDialogComponent,
    // Device Groups
    CreateDeviceGroupComponent,
    EditDeviceGroupComponent,
    // Devices
    CreateDeviceComponent,
    EditDeviceComponent,
    // StopTypes
    CreateStopTypeComponent,
    EditStopTypeComponent,
    // Stops
    CreateStopComponent,
    EditStopComponent 
  ]
})
export class AppModule {}

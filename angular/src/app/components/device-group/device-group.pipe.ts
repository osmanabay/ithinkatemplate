import { Pipe, PipeTransform } from '@angular/core';
import { DeviceGroupFullOutPut } from '@app/services/device-group/dtos/DeviceGroupFullOutPut';
import { DepartmentFullOutPut } from '@app/services/department/dtos/DepartmentFullOutPut';

@Pipe({
  name: 'deviceGroupFilterByName'
})
export class DeviceGroupsPipe implements PipeTransform {

  transform(deviceGroups: DeviceGroupFullOutPut[], department: DepartmentFullOutPut): DeviceGroupFullOutPut[] {
    if (!deviceGroups || department == null) {
      return deviceGroups;
    }
    return deviceGroups.filter(deviceGroup => deviceGroup.department.id == department.id);
  }
}

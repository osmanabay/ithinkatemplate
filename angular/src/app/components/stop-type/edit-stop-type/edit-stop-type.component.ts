import { Component, OnInit, Injector, Inject, Optional } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { StopTypeFullOutPut } from '@app/services/stop-type/dtos/StopTypeFullOutPut';
import { StopTypeService } from '@app/services/stop-type/stop-type.service';
import { GetStopTypeInput } from '@app/services/stop-type/dtos/GetStopTypeInput';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-edit-stop-type',
  templateUrl: './edit-stop-type.component.html',
  styleUrls: ['./edit-stop-type.component.css']
})
export class EditStopTypeComponent extends AppComponentBase implements OnInit {
  saving = false;
  stopType: StopTypeFullOutPut = new StopTypeFullOutPut();;

  constructor(injector: Injector,
    public _stopTypeService: StopTypeService,
    private _dialogRef: MatDialogRef<EditStopTypeComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) private _stopType: GetStopTypeInput) {
    super(injector);
  }

  ngOnInit() {
    this._stopTypeService.get(this._stopType).subscribe((result: StopTypeFullOutPut) => {
      this.stopType = result;
    });
  }

  save(): void {
    this.saving = true;
    // let updateDepartmentInput = new UpdateDepartmentInput();
    // updateDepartmentInput.id = this.department.id;
    // updateDepartmentInput.name = this.department.name;
    this._stopTypeService
      .update({name:this.stopType.name, id: this.stopType.id})
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.close(true);
      });
  }

  close(result: any): void {
    this._dialogRef.close(result);
  }
}


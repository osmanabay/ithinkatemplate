import { Component, Injector, Inject, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { finalize } from 'rxjs/operators';
import { HttpClient } from "@angular/common/http";

import DataSource from "devextreme/data/data_source";
import CustomStore from 'devextreme/data/custom_store';
import { AppComponentBase } from '@shared/app-component-base';
import { ModalManagerService } from '@app/services/common/modal-manager.service';
import { StopFullOutPut } from '@app/services/stop/dtos/StopFullOutPut';
import { StopService } from '@app/services/stop/stop.service';
import { DeleteStopInput } from '@app/services/stop/dtos/DeleteStopInput';
import { appModuleAnimation } from '@shared/animations/routerTransition';

@Component({
  selector: 'app-stop',
  templateUrl: './stop.component.html',
  animations: [appModuleAnimation()],
  styleUrls: ['./stop.component.css']
})
export class StopComponent extends AppComponentBase implements OnInit {
  stops: StopFullOutPut[] = [];
  dataSource: any = {};

  constructor(
    injector: Injector,
    private _stopService: StopService,
    private _modelManagerService: ModalManagerService,
    private _dialog: MatDialog,
    @Inject(HttpClient) httpClient: HttpClient
  ) {
    super(injector);
  }
  ngOnInit(): void {
    this.dataSource = this.createDxDataSource();
  }

  createStop(): void {
    this._modelManagerService.openCreateStopDialog().afterClosed().subscribe(result => {
      if (result) {
        this.refreshDataGrid();
      }
    });
  }

  editStop(id: number): void {
    this._modelManagerService.openEditStopDialog(id).afterClosed().subscribe(result => {
      if (result) {
        this.refreshDataGrid();
      }
    });
  }

  deleteStop(stop: DeleteStopInput): void {
    abp.message.confirm(
      this.l('StopDeleteWarningMessage', stop.name),
      this.l("AreYouSureWarningMessage"),
      (result: boolean) => {
        if (result) {
          this._stopService
            .delete(stop)
            .pipe(
              finalize(() => {
                abp.notify.success(this.l('SuccessfullyDeleted'));
                this.refreshDataGrid();
              })
            )
            .subscribe(() => { });
        }
      }
    );
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift(
      {
        location: 'before',
        template: 'formNameTemplate'
      },
      {
        location: 'after',
        template: 'refreshButtonTemplate'
      });
  }

  // dxGrid Changed
  onOptionChanged(e){
    // console.log(e);
    
  }

  refreshDataGrid() {
    this.dataSource.reload();
  }
  
  createDxDataSource(): DataSource {
    return new DataSource({
      store: new CustomStore({
        key: "id",
        loadMode: "raw",
        load: () => {
          return new Promise((resolve, reject) => {
            this._stopService
              .getList()
              .pipe(
                finalize(() => {
                  reject();
                  //finishedCallback();
                })
              )
              .subscribe((result: StopFullOutPut[]) => {
                this.stops = result;
                resolve(this.stops);
              });
          });
        }
      })
    });
  }
}


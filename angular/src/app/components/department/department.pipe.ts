import { Pipe, PipeTransform } from '@angular/core';
import { DepartmentFullOutPut } from '@app/services/department/dtos/DepartmentFullOutPut';
import { FactoryFullOutPut } from '@app/services/factory/dtos/FactoryFullOutPut';

@Pipe({
  name: 'departmentFilterByName'
})
export class DepartmentPipe implements PipeTransform {

  transform(departments: DepartmentFullOutPut[], factory: FactoryFullOutPut): DepartmentFullOutPut[] {
    if(!departments || factory == null){
      return departments;
    }
    return departments.filter(department=> department.factory.id == factory.id);
  }

}

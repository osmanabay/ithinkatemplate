import { Component, OnInit, Injector, Inject } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/app-component-base';
import { ModalManagerService } from '@app/services/common/modal-manager.service';
import { MatDialog } from '@angular/material';
import { HttpClient } from '@angular/common/http';
import DataSource from 'devextreme/data/data_source';
import CustomStore from 'devextreme/data/custom_store';
import { finalize } from 'rxjs/operators';
import { DeviceFullOutPut } from '@app/services/device/dtos/DeviceFullOutPut';
import { DeleteDeviceInput } from '@app/services/device/dtos/DeleteDeviceInput';
import { DeviceService } from '@app/services/device/device.service';

@Component({
  selector: 'app-device',
  templateUrl: './device.component.html',
  animations: [appModuleAnimation()],
  styleUrls: ['./device.component.css'],
  providers: []
})
export class DeviceComponent extends AppComponentBase implements OnInit {
  devices: DeviceFullOutPut[] = [];
  dataSource: any = {};

  constructor(
    injector: Injector,
    private _deviceService: DeviceService,
    private _modelManagerService: ModalManagerService,
    private _dialog: MatDialog,
    @Inject(HttpClient) httpClient: HttpClient
  ) {
    super(injector);
  }
  ngOnInit(): void {
    this.dataSource = this.createDxDataSource();
  }

  createDevice(): void {
    this._modelManagerService.openCreateDeviceDialog().afterClosed().subscribe(result => {
      if (result) {
        this.refreshDataGrid();
      }
    });
  }

  editDevice(id: number): void {
    this._modelManagerService.openEditDeviceDialog(id).afterClosed().subscribe(result => {
      if (result) {
        this.refreshDataGrid();
      }
    });
  }

  deleteDevice(device: DeleteDeviceInput): void {
    abp.message.confirm(
      this.l('DeviceDeleteWarningMessage', device.name),
      this.l("AreYouSureWarningMessage"),
      (result: boolean) => {
        if (result) {
          this._deviceService
            .delete(device)
            .pipe(
              finalize(() => {
                abp.notify.success(this.l('SuccessfullyDeleted'));
                this.refreshDataGrid();
              })
            )
            .subscribe(() => { });
        }
      }
    );
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift(
      {
        location: 'before',
        template: 'formNameTemplate'
      },
      {
        location: 'after',
        template: 'refreshButtonTemplate'
      });
  }

  // dxGrid Changed
  onOptionChanged(e){
    // console.log(e);
    
  }

  refreshDataGrid() {
    this.dataSource.reload();
  }
  
  createDxDataSource(): DataSource {
    return new DataSource({
      store: new CustomStore({
        key: "id",
        loadMode: "raw",
        load: () => {
          return new Promise((resolve, reject) => {
            this._deviceService
              .getList()
              .pipe(
                finalize(() => {
                  reject();
                  //finishedCallback();
                })
              )
              .subscribe((result: DeviceFullOutPut[]) => {
                this.devices = result;
                resolve(this.devices);
              });
          });
        }
      })
    });
  }
}


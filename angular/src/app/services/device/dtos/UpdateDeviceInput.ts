import { DeviceGroupFullOutPut } from "@app/services/device-group/dtos/DeviceGroupFullOutPut";

export class UpdateDeviceInput implements IUpdateDeviceInput {
    name: string;
    deviceNo: string;
    isOpened: boolean;
    id: number;
    deviceGroup: DeviceGroupFullOutPut;
}

export interface IUpdateDeviceInput {
    name: string;
    deviceNo: string;
    isOpened: boolean;
    id: number;
    deviceGroup: DeviceGroupFullOutPut
}

import { DeviceGroupPartOutPut } from "@app/services/device-group/dtos/deviceGroupPartOutPut";

export class CreateDeviceInput implements ICreateDeviceInput {
    name: string;
    deviceNo: string;
    isOpened: boolean;
    deviceGroup: DeviceGroupPartOutPut;
}

export interface ICreateDeviceInput {
    name: string;
    deviceNo: string;
    isOpened: boolean;
    deviceGroup: DeviceGroupPartOutPut
}
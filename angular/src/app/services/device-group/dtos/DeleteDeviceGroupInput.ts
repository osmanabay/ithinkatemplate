export class DeleteDeviceGroupInput implements IDeleteDeviceGroupInput {
    name: string;
    id: number;
}

export interface IDeleteDeviceGroupInput {
    name: string;
    id: number;
}
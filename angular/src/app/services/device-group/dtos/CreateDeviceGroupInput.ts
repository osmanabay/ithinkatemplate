import { DepartmentPartOutPut } from "@app/services/department/dtos/DepartmentPartOutPut";

export class CreateDeviceGroupInput implements ICreateDeviceGroupInput {
    name: string;
    ipAddress: string;
    department: DepartmentPartOutPut;
}

export interface ICreateDeviceGroupInput {
    name: string;
    ipAddress: string;
    department: DepartmentPartOutPut
}
export class GetDeviceGroupInput implements IGetDeviceGroupInput {
    id: number;
}

export interface IGetDeviceGroupInput {
    id: number;
}
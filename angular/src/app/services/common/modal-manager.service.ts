import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';
import { CreateDepartmentDialogComponent } from '@app/components/department/create-department/create-department.component';
import { GetDepartmentInput } from '../department/dtos/GetDepartmentInput';
import { EditDepartmentDialogComponent } from '@app/components/department/edit-department/edit-department.component';
import { CreateFactoryDialogComponent } from '@app/components/factory/create-factory/create-factory-dialog.component';
import { EditFactoryDialogComponent } from '@app/components/factory/edit-factory/edit-factory-dialog.component';
import { GetFactoryInput } from '../factory/dtos/GetFactoryInput';
import { EditDeviceGroupComponent } from '@app/components/device-group/edit-device-group/edit-device-group.component';
import { CreateDeviceGroupComponent } from '@app/components/device-group/create-device-group/create-device-group.component';
import { GetDeviceGroupInput } from '../device-group/dtos/GetDeviceGroupInput';
import { GetDeviceInput } from '../device/dtos/GetDeviceInput';
import { CreateDeviceComponent } from '@app/components/device/create-device/create-device.component';
import { EditDeviceComponent } from '@app/components/device/edit-device/edit-device.component';
import { CreateStopTypeComponent } from '@app/components/stop-type/create-stop-type/create-stop-type.component';
import { GetStopTypeInput } from '../stop-type/dtos/GetStopTypeInput';
import { EditStopTypeComponent } from '@app/components/stop-type/edit-stop-type/edit-stop-type.component';
import { CreateStopComponent } from '@app/components/stop/create-stop/create-stop.component';
import { GetStopInput } from '../stop/dtos/GetStopInput';
import { EditStopComponent } from '@app/components/stop/edit-stop/edit-stop.component';

@Injectable({
  providedIn: 'root'
})
export class ModalManagerService {

  constructor(private _dialog: MatDialog, ) { }

  // Departments
  openCreateDepartmentDialog(parameters?: any): any {
    let createDepartmentDialog;
    createDepartmentDialog = this._dialog.open(CreateDepartmentDialogComponent, {
      data: parameters
    });

    return createDepartmentDialog;
  }

  openEditDepartmentDialog(id?: number): any {
    let editDepartmentDialog;
    let getDepartmentInput = new GetDepartmentInput();
    getDepartmentInput.id = id;

    editDepartmentDialog = this._dialog.open(EditDepartmentDialogComponent, {
      data: getDepartmentInput
    });

    return editDepartmentDialog;
  }

  // Factories
  openCreateFactoryDialog(parameters?: any): any {
    let createFactoryDialog;
    createFactoryDialog = this._dialog.open(CreateFactoryDialogComponent, {
      data: parameters
    });

    return createFactoryDialog;
  }

  openEditFactoryDialog(id?: number): any {
    let editFactoryDialog;
    let getFactoryInput = new GetFactoryInput();
    getFactoryInput.id = id;

    editFactoryDialog = this._dialog.open(EditFactoryDialogComponent, {
      data: getFactoryInput
    });

    return editFactoryDialog;
  }

  // DeviceGroups

  openCreateDeviceGroupDialog(parameters?: any): any {
    let createDeviceGroupDialog;
    createDeviceGroupDialog = this._dialog.open(CreateDeviceGroupComponent, {
      maxWidth: '100vw',
      maxHeight: '100vh',
      width: '750px',
      height: '85vh',
      disableClose: true,
      hasBackdrop: true,
      data: parameters
    });

    return createDeviceGroupDialog;
  }

  openEditDeviceGroupDialog(id?: number): any {
    let editDeviceGroupDialog;
    let getDeviceGroupInput = new GetDeviceGroupInput();
    getDeviceGroupInput.id = id;

    editDeviceGroupDialog = this._dialog.open(EditDeviceGroupComponent, {
      data: getDeviceGroupInput
    });

    return editDeviceGroupDialog;
  }

  // Device
  openCreateDeviceDialog(parameters?: any): any {
    let createDeviceDialog;
    createDeviceDialog = this._dialog.open(CreateDeviceComponent, {
      data: parameters
    });

    return createDeviceDialog;
  }

  openEditDeviceDialog(id?: number): any {
    let editDeviceDialog;
    let getDeviceInput = new GetDeviceInput();
    getDeviceInput.id = id;

    editDeviceDialog = this._dialog.open(EditDeviceComponent, {
      data: getDeviceInput
    });

    return editDeviceDialog;
  }

  // StopType
  openCreateStopTypeDialog(parameters?: any): any {
    let createStopTypeDialog;
    createStopTypeDialog = this._dialog.open(CreateStopTypeComponent, {
      data: parameters
    });

    return createStopTypeDialog;
  }

  openEditStopTypeDialog(id?: number): any {
    let editStopTypeDialog;
    let getStopTypeInput = new GetStopTypeInput();
    getStopTypeInput.id = id;

    editStopTypeDialog = this._dialog.open(EditStopTypeComponent, {
      data: getStopTypeInput
    });

    return editStopTypeDialog;
  }

  // Stop
  openCreateStopDialog(parameters?: any): any {
    let createStopDialog;
    createStopDialog = this._dialog.open(CreateStopComponent, {
      data: parameters
    });

    return createStopDialog;
  }

  openEditStopDialog(id?: number): any {
    let editStopDialog;
    let getStopInput = new GetStopInput();
    getStopInput.id = id;

    editStopDialog = this._dialog.open(EditStopComponent, {
      data: getStopInput
    });

    return editStopDialog;
  }
}

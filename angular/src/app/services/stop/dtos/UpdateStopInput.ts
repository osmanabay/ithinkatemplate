import { StopTypeFullOutPut } from "@app/services/stop-type/dtos/StopTypeFullOutPut";

export class UpdateStopInput implements IUpdateStopInput {
    name: string;
    duration: number;
    id: number;
    stopType: StopTypeFullOutPut;
}

export interface IUpdateStopInput {
    name: string;
    duration: number | undefined;
    id: number;
    stopType: StopTypeFullOutPut
}
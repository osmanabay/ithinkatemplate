export class DeleteStopInput implements IDeleteStopInput {
    name: string;
    id: number;
}

export interface IDeleteStopInput {
    name: string;
    id: number;
}

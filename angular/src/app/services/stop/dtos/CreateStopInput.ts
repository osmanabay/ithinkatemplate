import { StopTypeFullOutPut } from "@app/services/stop-type/dtos/StopTypeFullOutPut";

export class CreateStopInput implements ICreateStopInput {
    name: string;
    duration: number;
    id: number;
    stopType: StopTypeFullOutPut;
}

export interface ICreateStopInput {
    name: string;
    duration: number | undefined;
    id: number;
    stopType: StopTypeFullOutPut
}
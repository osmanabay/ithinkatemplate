export class FactoryPartOutPut implements IFactoryPartOutPut {
    id:number;
    name:string
}

export interface IFactoryPartOutPut{
    id:number;
    name:string;
}

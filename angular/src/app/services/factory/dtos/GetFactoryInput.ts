export class GetFactoryInput implements IGetFactoryInput {
    id: number;
}

export interface IGetFactoryInput {
    id: number;
}
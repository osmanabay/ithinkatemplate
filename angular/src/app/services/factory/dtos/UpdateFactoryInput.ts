export class UpdateFactoryInput implements IUpdateFactoryInput {
    name: string;
    id: number;
}

export interface IUpdateFactoryInput {
    name: string;
    id: number;
}
export class CreateFactoryInput implements ICreateFactoryInput {
    name: string;
    id: number;
}

export interface ICreateFactoryInput {
    name: string;
    id: number;
}

export class DeleteStopTypeInput implements IDeleteStopTypeInput {
    name: string;
    id: number;
}

export interface IDeleteStopTypeInput {
    name: string;
    id: number;
}

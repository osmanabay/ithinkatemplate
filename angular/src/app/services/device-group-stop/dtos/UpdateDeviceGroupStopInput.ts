import { DeviceGroupPartOutPut } from "@app/services/device-group/dtos/deviceGroupPartOutPut";
import { StopFullOutPut } from "@app/services/stop/dtos/StopFullOutPut";

export class UpdateDeviceGroupStopInput implements IUpdateDeviceGroupStopInput {
    id: number;
    starDate: Date;
    endDate: Date;
    deviceGroup: DeviceGroupPartOutPut;
    stop: StopFullOutPut;
}

export interface IUpdateDeviceGroupStopInput {
    id: number;
    starDate: Date;
    endDate: Date;
    deviceGroup: DeviceGroupPartOutPut;
    stop: StopFullOutPut;
}
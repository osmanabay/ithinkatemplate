export class DeleteDeviceGroupStopInput implements IDeleteDeviceGroupStopInput {
    id: number;
}

export interface IDeleteDeviceGroupStopInput {
    id: number;
}
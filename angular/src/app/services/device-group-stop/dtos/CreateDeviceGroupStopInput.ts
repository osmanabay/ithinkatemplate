import { DeviceGroupPartOutPut } from "@app/services/device-group/dtos/deviceGroupPartOutPut";
import { StopFullOutPut } from "@app/services/stop/dtos/StopFullOutPut";

export class CreateDeviceGroupStopInput implements IDeviceGroupStopService {
    starDate: Date;
    endDate: Date;
    deviceGroup: DeviceGroupPartOutPut;
    stop: StopFullOutPut;
}


export interface IDeviceGroupStopService {
    starDate: Date;
    endDate: Date;
    deviceGroup: DeviceGroupPartOutPut;
    stop: StopFullOutPut;
  }
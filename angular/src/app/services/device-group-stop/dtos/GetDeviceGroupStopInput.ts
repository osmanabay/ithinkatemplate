export class GetDeviceGroupStopInput implements IGetDeviceGroupStopInput {
    id: number;
}

export interface IGetDeviceGroupStopInput {
    id: number;
}

/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { DeviceGroupStopService } from './device-group-stop.service';

describe('Service: DeviceGroupStop', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DeviceGroupStopService]
    });
  });

  it('should ...', inject([DeviceGroupStopService], (service: DeviceGroupStopService) => {
    expect(service).toBeTruthy();
  }));
});

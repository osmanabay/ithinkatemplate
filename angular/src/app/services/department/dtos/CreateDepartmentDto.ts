import { FactoryPartOutPut } from "@app/services/factory/dtos/FactoryPartOutPut";

export class CreateDepartmentInput implements ICreateDepartmentInput {
    name: string;
    factory: FactoryPartOutPut;
    constructor(data?: ICreateDepartmentInput) { }
}

export interface ICreateDepartmentInput {
    name: string | undefined;
    factory: FactoryPartOutPut
}
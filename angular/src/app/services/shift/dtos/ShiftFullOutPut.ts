export class ShiftFullOutPut implements IShiftFullOutPut {
    id: number;
    name: string;
    startDate: Date;
    endDate: Date;
}

export interface IShiftFullOutPut {
    id: number;
    name: string;
    startDate: Date;
    endDate: Date;
}
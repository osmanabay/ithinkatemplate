export class DeleteShiftInput implements IDeleteShiftInput {
    id: number;
}

export interface IDeleteShiftInput {
    id: number;
}

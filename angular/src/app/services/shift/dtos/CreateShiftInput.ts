import { Data } from "@angular/router";

export class CreateShiftInput implements ICreateShiftInput {
    id: number;
    name: string;
    startDate: Date;
    endDate: Date;
}

export interface ICreateShiftInput {
    id: number;
    name: string;
    startDate: Date;
    endDate: Date;
}
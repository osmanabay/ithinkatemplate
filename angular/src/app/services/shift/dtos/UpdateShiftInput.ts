export class UpdateShiftInput implements IUpdateShiftInput {
    id: number;
    name: string;
    startDate: Date;
    endDate: Date;
}

export interface IUpdateShiftInput {
    id: number;
    name: string;
    startDate: Date;
    endDate: Date;
}
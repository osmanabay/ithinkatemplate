export class CreateTeamInput implements ICreateTeamInput {
    id: number;
    name: string;
}

export interface ICreateTeamInput {
    id: number;
    name: string;
}